-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 19, 2013 at 09:42 AM
-- Server version: 5.5.33a-MariaDB-log
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vol`
--

-- --------------------------------------------------------

--
-- Table structure for table `recap`
--

CREATE TABLE IF NOT EXISTS `recap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_avion` int(11) DEFAULT NULL,
  `cdb_jour_heures` int(11) NOT NULL,
  `cdb_jour_minutes` int(11) NOT NULL,
  `cdb_nuit_heures` int(11) NOT NULL,
  `cdb_nuit_minutes` int(11) NOT NULL,
  `opl_jour_heures` int(11) NOT NULL,
  `opl_jour_minutes` int(11) NOT NULL,
  `opl_nuit_heures` int(11) NOT NULL,
  `opl_nuit_minutes` int(11) NOT NULL,
  `dc_jour_heures` int(11) NOT NULL,
  `dc_jour_minutes` int(11) NOT NULL,
  `dc_nuit_heures` int(11) NOT NULL,
  `dc_nuit_minutes` int(11) NOT NULL,
  `instruct_jour_heures` int(11) NOT NULL,
  `instruct_jour_minutes` int(11) NOT NULL,
  `instruct_nuit_heures` int(11) NOT NULL,
  `instruct_nuit_minutes` int(11) NOT NULL,
  `simu_heures` int(11) NOT NULL,
  `simu_minutes` int(11) NOT NULL,
  `ifr_heures` int(11) NOT NULL,
  `ifr_minutes` int(11) NOT NULL,
  `arrivees_ifr` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_avion` (`type_avion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `recap`
--

INSERT INTO `recap` (`id`, `type_avion`, `cdb_jour_heures`, `cdb_jour_minutes`, `cdb_nuit_heures`, `cdb_nuit_minutes`, `opl_jour_heures`, `opl_jour_minutes`, `opl_nuit_heures`, `opl_nuit_minutes`, `dc_jour_heures`, `dc_jour_minutes`, `dc_nuit_heures`, `dc_nuit_minutes`, `instruct_jour_heures`, `instruct_jour_minutes`, `instruct_nuit_heures`, `instruct_nuit_minutes`, `simu_heures`, `simu_minutes`, `ifr_heures`, `ifr_minutes`, `arrivees_ifr`) VALUES
(1, 5, 0, 0, 0, 0, 914, 4, 803, 53, 0, 0, 0, 0, 0, 0, 0, 0, 61, 0, 1746, 57, 227),
(2, 6, 0, 0, 0, 0, 845, 38, 752, 57, 0, 0, 0, 0, 0, 0, 0, 0, 47, 30, 1637, 23, 229),
(3, 7, 118, 42, 34, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 153, 23, 94),
(4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 8, 657, 50, 51, 40, 75, 5, 4, 25, 64, 15, 7, 0, 613, 5, 0, 0, 60, 15, 19, 35, 146),
(9, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60, 0, 0, 0, 0),
(10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 42, 45, 0, 0, 0, 0, 0, 0, 30, 0, 37, 30, 62),
(11, 11, 1043, 15, 65, 5, 187, 20, 7, 40, 96, 55, 7, 40, 884, 30, 0, 0, 62, 30, 115, 55, 181),
(12, 12, 1, 30, 0, 0, 31, 15, 2, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4),
(13, 13, 47, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 14, 23, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 15, 0, 40, 0, 0, 0, 0, 0, 0, 3, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(16, 16, 4, 45, 0, 0, 0, 0, 0, 0, 44, 20, 0, 0, 0, 55, 0, 0, 0, 0, 0, 0, 0),
(17, 17, 10, 5, 0, 0, 0, 0, 0, 0, 0, 30, 0, 0, 10, 5, 0, 0, 0, 0, 0, 0, 0),
(18, 18, 21, 50, 0, 0, 0, 0, 0, 0, 1, 40, 0, 0, 20, 35, 0, 0, 0, 0, 0, 0, 0),
(19, 19, 4, 35, 0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 20, 190, 30, 0, 0, 0, 0, 0, 0, 2, 25, 0, 0, 161, 50, 0, 0, 0, 0, 0, 0, 0),
(21, 21, 917, 30, 65, 10, 0, 0, 0, 0, 31, 35, 2, 0, 0, 0, 0, 0, 82, 0, 26, 0, 167),
(22, 22, 747, 55, 38, 10, 0, 0, 0, 0, 4, 30, 0, 0, 0, 0, 0, 0, 23, 0, 2, 30, 79),
(23, 23, 51, 30, 0, 0, 0, 0, 0, 0, 48, 10, 0, 0, 10, 30, 0, 0, 0, 0, 0, 0, 0),
(24, 24, 27, 25, 0, 0, 0, 0, 0, 0, 0, 40, 0, 0, 25, 15, 0, 0, 0, 0, 0, 0, 0),
(25, 25, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 26, 1, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 55, 0, 0, 0, 0, 0, 0, 0),
(27, 27, 259, 0, 0, 0, 0, 0, 0, 0, 20, 35, 0, 0, 201, 10, 0, 0, 0, 0, 0, 0, 0),
(28, 28, 0, 0, 0, 0, 0, 0, 0, 0, 6, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 29, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 30, 7, 35, 0, 0, 0, 0, 0, 0, 2, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `recap`
--
ALTER TABLE `recap`
  ADD CONSTRAINT `types_avions_id_avion` FOREIGN KEY (`type_avion`) REFERENCES `types_avion` (`id_avion`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
