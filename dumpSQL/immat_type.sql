-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Jeu 14 Novembre 2013 à 15:18
-- Version du serveur: 5.5.33a-MariaDB-log
-- Version de PHP: 5.5.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `vol`
--

-- --------------------------------------------------------

--
-- Structure de la table `immat_type`
--

CREATE TABLE IF NOT EXISTS `immat_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `immatriculation` varchar(8) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=185 ;

--
-- Contenu de la table `immat_type`
--

INSERT INTO `immat_type` (`id`, `immatriculation`, `type`) VALUES
(1, 'FGFKA', 3),
(2, 'FGFKB', 3),
(3, 'FGFKD', 3),
(4, 'FGFKE', 3),
(5, 'FGFKF', 3),
(6, 'FGFKG', 3),
(7, 'FGFKH', 3),
(8, 'FGFKI', 3),
(9, 'FGFKJ', 3),
(10, 'FGFKJ', 3),
(11, 'FGFKK', 3),
(12, 'FGFKL', 3),
(13, 'FGFKM', 3),
(14, 'FGFKN', 3),
(15, 'FGFKO', 3),
(16, 'FGFKP', 3),
(17, 'FGFKQ', 3),
(18, 'FGFKR', 3),
(19, 'FGFKS', 3),
(20, 'FGFKT', 3),
(21, 'FGFKU', 3),
(22, 'FGFKV', 3),
(23, 'FGFKX', 3),
(24, 'FGFKY', 3),
(25, 'FGFKZ', 3),
(26, 'FGGEA', 3),
(27, 'FGGEB', 3),
(28, 'FGGEC', 3),
(29, 'FGGEE', 3),
(30, 'FGGEF', 3),
(31, 'FGGEG', 3),
(32, 'FGHQA', 3),
(33, 'FGHQB', 3),
(34, 'FGHQC', 3),
(35, 'FGHQD', 3),
(36, 'FGHQE', 3),
(37, 'FGHQF', 3),
(38, 'FGHQG', 3),
(39, 'FGHQH', 3),
(40, 'FGHQI', 3),
(41, 'FGHQJ', 3),
(42, 'FGHQK', 3),
(43, 'FGHQL', 3),
(44, 'FGHQM', 3),
(45, 'FGHQO', 3),
(46, 'FGHQP', 3),
(47, 'FGHQQ', 3),
(48, 'FGHQR', 3),
(49, 'FGJVA', 3),
(50, 'FGJVB', 3),
(51, 'FGJVG', 3),
(52, 'FGJVW', 3),
(53, 'FGKXA', 3),
(54, 'FGKXB', 3),
(55, 'FGKXC', 3),
(56, 'FGKXD', 3),
(57, 'FGKXE', 3),
(58, 'FGKXF', 3),
(59, 'FGKXG', 3),
(60, 'FGKXH', 3),
(61, 'FGKXI', 3),
(62, 'FGKXJ', 3),
(63, 'FGKXK', 3),
(64, 'FGKXL', 3),
(65, 'FGKXM', 3),
(66, 'FGKXN', 3),
(67, 'FGKXO', 3),
(68, 'FGKXP', 3),
(69, 'FGKXQ', 3),
(70, 'FGKXR', 3),
(71, 'FGKXS', 3),
(72, 'FGKXT', 3),
(73, 'FGKXU', 3),
(74, 'FGKXV', 3),
(75, 'FGKXY', 3),
(76, 'FGKXZ', 3),
(77, 'FGLGG', 3),
(78, 'FGLGH', 3),
(79, 'FGLGM', 3),
(80, 'FGMZA', 4),
(81, 'FGMZB', 4),
(82, 'FGMZC', 4),
(83, 'FGMZD', 4),
(84, 'FGMZE', 4),
(85, 'FGPMA', 2),
(86, 'FGPMB', 2),
(87, 'FGPMC', 2),
(88, 'FGPMD', 2),
(89, 'FGPME', 2),
(90, 'FGPMF', 2),
(91, 'FGPMG', 2),
(92, 'FGRHA', 2),
(93, 'FGRHB', 2),
(94, 'FGRHC', 2),
(95, 'FGRHD', 2),
(96, 'FGRHE', 2),
(97, 'FGRHF', 2),
(98, 'FGRHG', 2),
(99, 'FGRHH', 2),
(100, 'FGRHI', 2),
(101, 'FGRHJ', 2),
(102, 'FGRHK', 2),
(103, 'FGRHL', 2),
(104, 'FGRHM', 2),
(105, 'FGRHN', 2),
(106, 'FGRHO', 2),
(107, 'FGRHP', 2),
(108, 'FGRHQ', 2),
(109, 'FGRHR', 2),
(110, 'FGRHS', 2),
(111, 'FGRHT', 2),
(112, 'FGRHU', 2),
(113, 'FGRHV', 2),
(114, 'FGRHX', 2),
(115, 'FGRHY', 2),
(116, 'FGRHZ', 2),
(117, 'FGRXA', 2),
(118, 'FGRXB', 2),
(119, 'FGRXC', 2),
(120, 'FGRXD', 2),
(121, 'FGRXE', 2),
(122, 'FGRXF', 2),
(123, 'FGRXG', 2),
(124, 'FGRXH', 2),
(125, 'FGRXI', 2),
(126, 'FGRXJ', 2),
(127, 'FGRXK', 2),
(128, 'FGRXL', 2),
(129, 'FGRXM', 2),
(130, 'FGRXN', 2),
(131, 'FGRXR', 2),
(132, 'FGTAD', 4),
(133, 'FGTAE', 4),
(134, 'FGTAH', 4),
(135, 'FGTAI', 4),
(136, 'FGTAJ', 4),
(137, 'FGTAK', 4),
(138, 'FGTAL', 4),
(139, 'FGTAM', 4),
(140, 'FGTAN', 4),
(141, 'FGTAO', 4),
(142, 'FGTAP', 4),
(143, 'FGTAQ', 4),
(144, 'FGTAR', 4),
(145, 'FGTAS', 4),
(146, 'FGTAT', 4),
(147, 'FGTAU', 4),
(148, 'FGTAV', 4),
(149, 'FGTAX', 4),
(150, 'FGTAY', 4),
(151, 'FGTAZ', 4),
(152, 'FGUGA', 1),
(153, 'FGUGB', 1),
(154, 'FGUGC', 1),
(155, 'FGUGD', 1),
(156, 'FGUGE', 1),
(157, 'FGUGF', 1),
(158, 'FGUGG', 1),
(159, 'FGUGH', 1),
(160, 'FGUGI', 1),
(161, 'FGUGJ', 1),
(162, 'FGUGK', 1),
(163, 'FGUGL', 1),
(164, 'FGUGM', 1),
(165, 'FGUGN', 1),
(166, 'FGUGO', 1),
(167, 'FGUGP', 1),
(168, 'FGUGQ', 1),
(169, 'FGUGR', 1),
(170, 'FHBNA', 3),
(171, 'FHBNC', 3),
(172, 'FHBND', 3),
(173, 'FHBNG', 3),
(174, 'FHEPA', 3),
(175, 'FHEPB', 3),
(176, 'FHEPC', 3),
(177, 'FHEPD', 3),
(178, 'FHEPE', 3),
(179, 'simuA318', 1),
(180, 'simuA319', 2),
(181, 'simuA320', 3),
(182, 'simuA321', 4);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `immat_type`
--
ALTER TABLE `immat_type`
  ADD CONSTRAINT `types_avion_id_avion` FOREIGN KEY (`type`) REFERENCES `types_avion` (`id_avion`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
