-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 19, 2013 at 09:42 AM
-- Server version: 5.5.33a-MariaDB-log
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vol`
--

-- --------------------------------------------------------

--
-- Table structure for table `types_avion`
--

CREATE TABLE IF NOT EXISTS `types_avion` (
  `id_avion` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  `type_avion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_avion`),
  KEY `types_avion` (`type_avion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `types_avion`
--

INSERT INTO `types_avion` (`id_avion`, `type`, `type_avion`) VALUES
(1, 'A318', 2),
(2, 'A319', 2),
(3, 'A320', 2),
(4, 'A321', 2),
(5, 'A340', 2),
(6, 'A341', 2),
(7, 'A342', 2),
(8, 'AJET', 2),
(9, 'B737', 2),
(10, 'BE55', 2),
(11, 'CM170', 2),
(12, 'M20', 2),
(13, 'C152', 1),
(14, 'C172', 1),
(15, 'C177', 1),
(16, 'CAP10', 1),
(17, 'CO02', 1),
(18, 'Divers', 1),
(19, 'DR360', 1),
(20, 'DR400', 1),
(21, 'F1CR', 1),
(22, 'F1CT', 1),
(23, 'HR200', 1),
(24, 'J3', 1),
(25, 'M201', 1),
(26, 'M893', 1),
(27, 'MS880', 1),
(28, 'R1180', 1),
(29, 'R2100', 1),
(30, 'TB10', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `types_avion`
--
ALTER TABLE `types_avion`
  ADD CONSTRAINT `nb_moteurs_type_avion` FOREIGN KEY (`type_avion`) REFERENCES `nb_moteurs_avion` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
