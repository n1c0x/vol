-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 19, 2013 at 09:42 AM
-- Server version: 5.5.33a-MariaDB-log
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vol`
--

-- --------------------------------------------------------

--
-- Table structure for table `nb_moteurs_avion`
--

CREATE TABLE IF NOT EXISTS `nb_moteurs_avion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nb_moteurs_avion` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nb_moteurs_avion` (`nb_moteurs_avion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `nb_moteurs_avion`
--

INSERT INTO `nb_moteurs_avion` (`id`, `nb_moteurs_avion`) VALUES
(1, 'Monomoteur'),
(2, 'Multimoteur');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
