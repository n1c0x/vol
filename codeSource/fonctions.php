<?php
require_once('connect.php');

# Comptage du nombre d'avions différents
function count_avion($con){
	$sql = "SELECT count(*) FROM types_avion";
	$nombre_avions = array();
	$query = $con->query($sql);
	while($result=$query->fetch(PDO::FETCH_NUM)){
		$nombre_avions[]=$result[0];
	}
	return $nombre_avions[0];
}

# Affiche tous les avions, leur immatriculation et type
function all_avions($con){
	$sql = "SELECT immat.id_immat,immat.immat, type_avion.type FROM immat
			JOIN type_avion
			ON immat.type_avion_id=type_avion.id_avion
			ORDER BY immat.id_immat";
	$query = $con->query($sql);
	return $query;
}

# Affiche tous les pilotes
function all_pilote($con){
	$sql = "SELECT * FROM pilote
			ORDER BY pilote.nom_pilote";
	$query = $con->query($sql);
	return $query;
}

# Affiche tous les postes
function all_poste($con){
	$sql = "SELECT * FROM poste WHERE poste_abbr='cdb' OR poste_abbr='opl'";
	$query = $con->query($sql);
	return $query;
}

# Affiche tous les codes aéroport
function all_aita($con){
	$sql = "SELECT * FROM aita ORDER BY code_aita";
	$query = $con->query($sql);
	return $query;
}

# Affiche toutes les fonctions
function all_fonction($con){
	$sql = "SELECT * FROM fonction";
	$query = $con->query($sql);
	return $query;
}

# Affiche les types d'avions
function type_avion($con,$type=null){
	$sql = "SELECT * FROM type_avion";
	if (!empty($type)) {
		$sql .= " WHERE type_avion.type='".$type."'";
		$query = $con->query($sql);
		while($result=$query->fetch(PDO::FETCH_NUM)){$types_avion[] = $result;}
		return $types_avion[0];
	}else {
		$types_avion = array();
		$query = $con->query($sql);
		while($result=$query->fetch(PDO::FETCH_NUM)){$types_avion[] = $result;}
		return $types_avion;
	}
}

# Affichage de tous les vols
function all_vols($con){
	$sql = "SELECT
			vols.id_vol,
			DATE_FORMAT(vols.date_vol,'%d/%m/%Y') AS date,
			vols.cdb_id,
			vols.opl_id,
			vols.obs1_id,
			vols.obs2_id,
			vols.instructeur_id,
			vols.depart_aita_id,
			vols.arrivee_aita_id,
			vols.duree_jour,
			vols.duree_nuit,
			vols.arrivee_ifr,
			vols.fonction_id,
			vols.poste_id,
			vols.immat_id,
			vols.observations,
			vols.simu,
			vols.dc,
			vols.ifr_vfr_id,
			vols.user_id,
			immat.immat,
			ADDTIME(vols.duree_jour,vols.duree_nuit) AS duree_simu,
			ADDTIME(vols.duree_jour,vols.duree_nuit) AS duree_dc,
			ADDTIME(vols.duree_jour,vols.duree_nuit) AS duree_ifr,
			type_avion.type AS type_avion,
			aita.code_aita AS code_aita_depart,
			aita2.code_aita AS code_aita_arrivee,
			poste.poste AS poste_long,
			fonction.fonction AS fonction
			FROM vols
			JOIN immat
			ON vols.immat_id=immat.id_immat
			JOIN type_avion
			ON immat.type_avion_id=type_avion.id_avion
			JOIN aita
			ON vols.depart_aita_id=aita.id_aita
			JOIN aita aita2
			ON vols.arrivee_aita_id=aita2.id_aita
			JOIN poste
			ON vols.poste_id=poste.id_poste
			JOIN fonction
			ON vols.fonction_id=fonction.id_fonction
			ORDER BY vols.id_vol";
	$query = $con->query($sql);
	return $query;
}

# Somme des heures de vol de simu en fonction de l'avion, si présent
function somme_heures_vols_simu($con,$type=null){
	$sql = "SELECT
				SEC_TO_TIME(SUM(TIME_TO_SEC(vols.duree_jour))) AS duree_jour,
				SEC_TO_TIME(SUM(TIME_TO_SEC(vols.duree_nuit))) AS duree_nuit,
				type_avion.type
				FROM vols
				JOIN immat
				ON vols.immat_id=immat.id_immat
				JOIN type_avion
				ON immat.type_avion_id=type_avion.id_avion
				WHERE vols.simu = '1'";
	if (!empty($type)) {
		$sql .= " AND type_avion.type='".$type."'";
	}
	$query = $con->query($sql);
	while($result=$query->fetch(PDO::FETCH_ASSOC)){
		$somme_heures_vols[]=$result;
	}
	return $somme_heures_vols;
}

# Somme des heures de vol de double commande en fonction du type d'avion
function somme_heures_vols_dc ($con,$type=null){
	$sql = "SELECT
				SEC_TO_TIME(SUM(TIME_TO_SEC(vols.duree_jour))) AS duree_jour,
				SEC_TO_TIME(SUM(TIME_TO_SEC(vols.duree_nuit))) AS duree_nuit,
				type_avion.type
				FROM vols
				JOIN immat
				ON vols.immat_id=immat.id_immat
				JOIN type_avion
				ON immat.type_avion_id=type_avion.id_avion
				WHERE vols.dc = '1'";
	if (!empty($type)) {
		$sql .= " AND type_avion.type='".$type."'";
	}
	$query = $con->query($sql);
	while($result=$query->fetch(PDO::FETCH_ASSOC)){$somme_heures_vols[] = $result;}
	return $somme_heures_vols;
}

# Somme des heures de vol IFR en fonction du type d'avion
function somme_heures_vols_ifr ($con,$type=null){
	$sql = "SELECT
				ADDTIME(vols.duree_jour,vols.duree_nuit) AS duree
				FROM vols
				JOIN immat
				ON vols.immat_id=immat.id_immat
				JOIN type_avion
				ON immat.type_avion_id=type_avion.id_avion";
	if (!empty($type)) {
		$sql .= " WHERE type_avion.type='".$type."'";
	}
	$query = $con->query($sql);
	while($result=$query->fetch(PDO::FETCH_ASSOC)){$somme_heures_vols_ifr[] = $result;}
	return $somme_heures_vols_ifr;
}

# Somme des heures de vol en fonction du poste et du type d'avion
function somme_heures_vols($con,$poste=null,$type=null){
	$sql = "SELECT
					type_avion.type,
					poste.poste_abbr,
					SEC_TO_TIME(SUM(TIME_TO_SEC(vols.duree_jour))) AS duree_jour,
					SEC_TO_TIME(SUM(TIME_TO_SEC(vols.duree_nuit))) AS duree_nuit
					FROM vols
					JOIN poste
					ON vols.poste_id=poste.id_poste
					JOIN immat
					ON vols.immat_id=immat.id_immat
					JOIN type_avion
					ON immat.type_avion_id=type_avion.id_avion
					WHERE vols.simu = null";
	if (!empty($poste) && !empty($type)) {
		$sql .= " AND poste.poste_abbr='".$poste."' AND type_avion.type='".$type."'";
	}elseif (!empty($type)) {
		$sql .= " AND type_avion.type='".$type."'";
	}elseif (!empty($poste)) {
		$sql .= " AND poste.poste_abbr='".$poste."'";
	}
	$query = $con->query($sql);
	while($result=$query->fetch(PDO::FETCH_ASSOC)){$types_avion[] = $result;}
	return $types_avion;
}

# Somme de toutes les heures de vol, en fonction dy type d'avion
function all_heures_vols($con, $type=null){
	$sql = "SELECT
					SEC_TO_TIME(SUM(TIME_TO_SEC(duree))) AS duree_all
					FROM (
						SELECT
					    ADDTIME(vols.duree_jour,vols.duree_nuit) AS duree
					    FROM vols
					    JOIN immat
					    ON vols.immat_id=immat.id_immat
					    JOIN type_avion
					    ON immat.type_avion_id=type_avion.id_avion";
	if (!empty($type)) {
		$sql .= " WHERE type_avion.type='".$type."'";
	}
	$sql .= ") AS S";
	// return $sql;
	$query = $con->query($sql);
	while($result=$query->fetch(PDO::FETCH_ASSOC)){$all_heures_vols[] = $result;}
	return $all_heures_vols;
}

# Somme des heures de vol en fonction du poste et du type d'avion
function count_ifr($con,$type=null){
	$sql = "SELECT
					type_avion.type,
					SUM(arrivee_ifr) AS arrivee_ifr
					FROM vols
					JOIN immat
					ON vols.immat_id=immat.id_immat
					JOIN type_avion
					ON immat.type_avion_id=type_avion.id_avion";
	if (!empty($type)) {
		$sql .= " WHERE type_avion.type='".$type."'";
	}
	$query = $con->query($sql);
	while($result=$query->fetch(PDO::FETCH_ASSOC)){
		$somme_ifr[]=$result;
	}
	return $somme_ifr;
}

# Ajout d'un zero si la valeur est inférieur à 10
function add_zero($input){
	if($input<10){$input="0".$input;}
	return $input;
}

# Conversion d'heures en minutes
function convertToHoursMins($time) {
	settype($time, 'integer');
	if ($time < 1) {
		return;
	}
	$result[0] = floor($time / 60);
	$result[1] = ($time % 60);
	return $result;
}

# Ajout du code Aita dans la BDD s'il n'est pas présent
function addAitaBDD($con,$aita,$aita_si_inconnu=null){
	/* Lecture du contenu de la table contenant les code AITA */
	$sql = "SELECT * FROM aita WHERE code_aita=:code_aita";
	$query_prepare_select = $con->prepare($sql);
	if ($aita_si_inconnu == null) {
		$aita_si_inconnu = $aita;
	}
	if ($aita != 0) {
		$depart = $aita;
	}
	else{
		$query_prepare_select->bindParam(':code_aita',$aita_si_inconnu,PDO::PARAM_STR,4);
		$query_prepare_select->execute();
		while($row = $query_prepare_select->fetch(PDO::FETCH_NUM)){
			$code[] = $row;
		}
		if (strcmp($code[0][1], $aita_si_inconnu) == 0 ) {
			$depart = $code[0][0];
		}
		else{
			/* Sauvegarde du code AITA du départ s'il n'existe pas encore dans la base de donnéees */
			$query_prepare = $con->prepare("INSERT INTO aita(code_aita) VALUES (:aita)");
			$query_prepare->bindParam(':aita',$aita_si_inconnu, PDO::PARAM_STR,4);
			$query_prepare->execute();

			$query_prepare_select->bindParam(':code_aita',$aita_si_inconnu,PDO::PARAM_STR,4);
			$query_prepare_select->execute();
			while ($row = $query_prepare_select->fetch(PDO::FETCH_NUM)) {
				$depart = $row[0];
			}
		}
	}
	return $depart;
}

# Selection de l'id de l'avion en fonction du type
function id_avion_type($con,$type){
	$sql = "SELECT id_avion FROM vol.type_avion WHERE type=:type";
	$query_prepare = $con->prepare($sql);
	$query_prepare->bindParam(':type',$type,PDO::PARAM_STR,10);
	$query_prepare->execute();
	while($row = $query_prepare->fetch(PDO::FETCH_NUM)){$id_avion[] = $row;}
	return $id_avion;
}

# Ajout de l'immatriculation dans la BDD si elle n'est pas présente
function addImmatBDD($con,$immat_select,$immat_si_inconnu,$type_select){
	if ($immat_si_inconnu == null) {
		$immat_si_inconnu = $immat_select;
	}
	if($immat_select != 0){
		$immat_id = $immat_select;
	}
	else{
		/* Lecture du contenu de la table contenant les immatriculations */
		$sql = "SELECT * FROM immat WHERE immat=:immat_type";
		$query_prepare = $con->prepare($sql);
		$query_prepare->bindParam(':immat_type',$immat_si_inconnu,PDO::PARAM_STR,5);
		$query_prepare->execute();
		while($row = $query_prepare->fetch(PDO::FETCH_NUM)){
			$immat[] = $row;
		}
		if (strcmp($immat[0][1], $immat_si_inconnu) != 0) {
			$query_prepare = $con->prepare("INSERT INTO immat(immat, type_avion_id) VALUES (:immat_type,:type_select)");
			if (isset($immat_si_inconnu) && $immat_si_inconnu !="") {
				$query_prepare->bindParam(':immat_type',$immat_si_inconnu, PDO::PARAM_STR,4);
				$id_avion_type = id_avion_type($con,$type_select);
				$query_prepare->bindParam(':type_select',$id_avion_type[0][0], PDO::PARAM_STR,4);
				$query_prepare->execute();
				$immat_id = $con->lastInsertId();
			}
		}
		else{
			$immat_id = $immat[0][0];
		}
	}
	return $immat_id;
}

# Ajout du pilote dans la BDD s'il n'est pas présent
function addpiloteBDD($con,$prenom,$nom){

	if($prenom_select != 0 && $nom_select != 0){
		$prenom_id = $prenom_select;
		$nom_id = $nom_select;
	}
	else{
		/* Lecture du contenu de la table contenant les immatriculations */
		$sql = "SELECT * FROM pilote WHERE prenom_pilote=:prenom AND nom_pilote=:nom";
		$query_prepare = $con->prepare($sql);
		$query_prepare->bindParam(':prenom',$prenom,PDO::PARAM_STR,5);
		$query_prepare->bindParam(':nom',$nom,PDO::PARAM_STR,5);
		$query_prepare->execute();
		while($row = $query_prepare->fetch(PDO::FETCH_NUM)){
			$pilote[] = $row;
		}
		if (strcmp($pilote[0][1], $immat_si_inconnu) != 0) {
			$query_prepare = $con->prepare("INSERT INTO immat(immat, type_avion_id) VALUES (:immat_type,:type_select)");
			if (isset($immat_si_inconnu) && $immat_si_inconnu !="") {
				$query_prepare->bindParam(':immat_type',$immat_si_inconnu, PDO::PARAM_STR,4);
				$query_prepare->bindParam(':type_select',$type_select, PDO::PARAM_STR,4);
				$query_prepare->execute();
				$immat_id = $con->lastInsertId();
			}
		}
		else{
			$immat_id = $immat[0][0];
		}
	}
}

?>
