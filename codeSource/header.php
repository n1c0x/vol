<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/design.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="js/jquery-ui/themes/themes/smoothness/jquery-ui.css">
	<script type="text/javascript" src="js/jquery-2.1.4.min.js" /></script>
	<script type="text/javascript" src="js/bootstrap.min.js" /></script>

	<script type="text/javascript" src="js/jquery.dataTables.min.js" /></script>
	<script type="text/javascript" src="js/jquery-ui/jquery-ui.js" /></script>
	<script type="text/javascript" src="js/script.js" /></script>

	<link rel="icon" href="img/LogoYvan.png" type="image/png" sizes="16x16" />
	<title>Plan de vol</title>
</head>

<body>
	<?php
require_once('fonctions.php');
date_default_timezone_set('Europe/Paris')
?>
		<nav class="navbar navbar-inverse">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">VolDB</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="?page=2">Ajouter un vol</a></li>
						<li><a href="?page=3">Afficher les vols</a></li>
						<li><a href="?page=7">Total</a></li>
						<li><a href="?page=8">Import Excel</a></li>
						<li><a href="?page=10">test</a></li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>