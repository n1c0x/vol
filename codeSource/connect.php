<?php
include('connect_infos.php');
//open a connection to a sql server
try{
	// Open a connection with MySQL database
	$con = new PDO('mysql:host='.$server.';dbname='.$db,$user,$pw);
	// Open a connection with sqlite database
	// $con = new PDO('sqlite:'.dirname(__FILE__).'/vol.sqlite');
	$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (Exception $e){
	die('Could not connect:'.$e->getMessage());
}

?>
