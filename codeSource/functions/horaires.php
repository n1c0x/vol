
<div class="container">
	<div class="row" id="blue">
	    <div class="col-md-12">
	        <a href="index.php"><img src="img/LogoYvan.png"/></a>
	    </div> <!-- end of span12 -->
	</div> <!-- end of row-fluid -->


<h1>Heures de vol par avion</h1>
<div class="table-responsive">
<?php
	// echo "<pre>";
	// print_r(type_avion($con));
	// echo "</pre>";

	$liste_types_avion = type_avion($con);
	foreach ($liste_types_avion as $key => $value) {
		$somme = array();
		?>
		<table class="table table-bordered">
			<tr>
				<th><?php echo $value[1].'<br>'; ?></th>
				<th>CDB<br />Jour</th>
				<th>CDB<br />Nuit</th>
				<th>OPL<br />Jour</th>
				<th>OPL<br />Nuit</th>
				<th>DC<br />Jour</th>
				<th>DC<br />Nuit</th>
				<th>INST<br />Jour</th>
				<th>INST<br />Nuit</th>
				<th>simu</th>
				<th>Arrivées<br />IFR</th>
				<th>Nb Arrivée<br />IFR</th>
				<th>Total</th>
			<tr>
				<td>Récap</td>
				<!-- Somme des deux autre lignes -->
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
			</tr>
			<tr>
				<td>Année en cours</td>
				<td>
					<?php
					if (!empty(somme_heures_vols($con,'cdb',$value[1])[0]['duree_jour'])) {
						echo somme_heures_vols($con,'cdb',$value[1])[0]['duree_jour'];
					}else {echo "00:00";}?>
				</td>
				<td>
					<?php
					if (!empty(somme_heures_vols($con,'cdb',$value[1])[0]['duree_nuit'])) {
						echo somme_heures_vols($con,'cdb',$value[1])[0]['duree_nuit'];
					}else {echo "00:00";}?>
				</td>
				<td>
					<?php
					if (!empty(somme_heures_vols($con,'opl',$value[1])[0]['duree_jour'])) {
						echo somme_heures_vols($con,'opl',$value[1])[0]['duree_jour'];
					}else {echo "00:00";}?>
				</td>
				<td>
					<?php
					if (!empty(somme_heures_vols($con,'opl',$value[1])[0]['duree_nuit'])) {
						echo somme_heures_vols($con,'opl',$value[1])[0]['duree_nuit'];
					}else {echo "00:00";}?>
				</td>
				<td>
					<?php
					if (!empty(somme_heures_vols_dc($con,$value[1])[0]['duree_jour'])) {
						echo somme_heures_vols_dc($con,$value[1])[0]['duree_jour'];
					} else {echo "00:00";}?>
				</td>
				<td>
					<?php
					if (!empty(somme_heures_vols_dc($con,$value[1])[0]['duree_nuit'])) {
						echo somme_heures_vols_dc($con,$value[1])[0]['duree_nuit'];
					}else {echo "00:00";}?>
				</td>
				<td>
					<?php
					if (!empty(somme_heures_vols($con,'instruct',$value[1])[0]['duree_jour'])) {
						echo somme_heures_vols($con,'instruct',$value[1])[0]['duree_jour'];
					}else {echo "00:00";}?>
				</td>
				<td>
					<?php
					if (!empty(somme_heures_vols($con,'instruct',$value[1])[0]['duree_nuit'])) {
						echo somme_heures_vols($con,'instruct',$value[1])[0]['duree_nuit'];
					}else {echo "00:00";}?>
				</td>
				<td>
					<?php
					if (!empty(somme_heures_vols_simu($con,$value[1])[0]['duree_jour'])) {
						echo somme_heures_vols_simu($con,$value[1])[0]['duree_jour'];
					}else {echo "00:00";}?>
				</td>
				<td>
					<?php
					if (!empty(somme_heures_vols_ifr($con,$value[1])[0]['duree'])) {
						echo somme_heures_vols_ifr($con,$value[1])[0]['duree'];
					}else {echo "00:00";}?>
				</td>
				<td>
					<?php
					if (!empty(count_ifr($con,$value[1])[0]['arrivee_ifr'])) {
						echo count_ifr($con,$value[1])[0]['arrivee_ifr'];
						} else {echo "0";}?>
				</td>
				<td>
				<?php
				if (!empty(all_heures_vols($con,$value[1])[0]['duree_all'])) {
					echo all_heures_vols($con,$value[1])[0]['duree_all'];
				}
				else {echo "00:00";}?>
				</td>
			</tr>
			<tr>
				<td>Au 31/12/<?php echo date('Y',strtotime("-1 year",strtotime('now')))?></td>
				<!-- Valeurs si année de date_vol = année - 1 -->
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
				<td>00:00</td>
			</tr>
		</table>
<?php
	}



echo "<h1>Aucune option</h1>";
echo "<pre>";
print_r(all_heures_vols($con, 'A320'));
echo "</pre>";
// echo "<h1>Juste Poste</h1>";
// echo "<pre>";
// print_r(somme_heures_vols($con,'cdb'));
// echo "</pre>";
// echo "<h1>Juste type</h1>";
// echo "<pre>";
// print_r(somme_heures_vols($con,null,'A319'));
// echo "</pre>";
// echo "<h1>Les deux</h1>";
// echo "<pre>";
// print_r(somme_heures_vols($con,'cdb','A319'));
// echo "</pre>";

?>
</div>
