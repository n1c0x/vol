<div class="span2">
    <table class="table table-bordered">
        <th>Immatriculation</th>
        <th>Type</th>
        <?php
            $query = all_avions($con);
            while($result=$query->fetch(PDO::FETCH_NUM)){ ?>
                <tr>
                    <td>
                        <?php echo $result[1]; ?>
                    </td>
                    <td>
                        <?php echo $result[2]; ?>
                    </td>
                </tr>
            <?php } ?>
    </table>
</div>
<div class="span2">
Types d'avions différents: <?php echo count_avion($con);?><br>
<table class="table table-bordered">
    <th>Type</th>
    <th>Nombre</th>
    <?php 
        for($i=0;$i<count(type_avion($con));$i++){
            echo "<tr><td>".type_avion($con)[$i][1]."</td><td>".count_immat($con,$i+1)."</td></tr>";
        }
    ?>
</table>
</div>