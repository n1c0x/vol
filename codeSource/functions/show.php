<div class="container-fluid">
	<div class="row" id="blue">
		<div class="col-md-12">
			<a href="index.php">
				<img src="img/LogoYvan.png" />
			</a>
		</div>
		<!-- end of span12 -->
	</div>
	<!-- end of row-fluid -->


	<h1>Vols</h1>
	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="tableau_vols">
			<thead>
				<th>Date</th>
				<th>Immatriculation</th>
				<th>Type d'avion</th>
				<th>Vol</th>
				<th>CDB</th>
				<th>OPL</th>
				<th>Observateur 1</th>
				<th>Observateur 2</th>
				<th>Instructeur</th>
				<th>Heures de vol Jour</th>
				<th>Heures de vol Nuit</th>
				<th>Simu</th>
				<th>Double Commande</th>
				<th>Heures de vol IFR</th>
				<th>Arrivée IFR</th>
				<th>Poste</th>
				<th>Fonction</th>
				<th>Observations</th>
			</thead>
			<tfoot>
				<th>Date</th>
				<th>Immatriculation</th>
				<th>Type d'avion</th>
				<th>Vol</th>
				<th>CDB</th>
				<th>OPL</th>
				<th>Observateur 1</th>
				<th>Observateur 2</th>
				<th>Instructeur</th>
				<th>Heures de vol Jour</th>
				<th>Heures de vol Nuit</th>
				<th>Simu</th>
				<th>Double Commande</th>
				<th>Heures de vol IFR</th>
				<th>Arrivée IFR</th>
				<th>Poste</th>
				<th>Fonction</th>
				<th>Observations</th>
			</tfoot>
			<tbody>
				<?php
		$query=all_vols($con);
		while($result = $query->fetch(PDO::FETCH_ASSOC)){
			// echo "<pre>";
			// print_r($result);
			// echo "</pre>"

			?>
					<tr>
						<td>
							<?php echo $result['date'] ?>
						</td>
						<td>
							<?php echo $result['immat'] ?>
						</td>
						<td>
							<?php echo $result['type_avion'] ?>
						</td>
						<td>
							<?php echo $result['code_aita_depart']."/".$result['code_aita_arrivee'] ?>
						</td>
						<td>
							<?php echo $result['cdb_id'] ?>
						</td>
						<td>
							<?php echo $result['opl_id'] ?>
						</td>
						<td>
							<?php echo $result['obs1_id'] ?>
						</td>
						<td>
							<?php echo $result['obs2_id'] ?>
						</td>
						<td>
							<?php echo $result['instructeur_id'] ?>
						</td>
						<?php
				if($result['simu'] == NULL){
					echo "<td>".$result['duree_jour']."</td><td>".$result['duree_nuit']."</td><td></td>";
				}else{
					echo "<td></td><td></td><td>".$result['duree_simu']."</td>";
				}
				if($result['dc'] == NULL){
					echo "<td></td>";
				}else{
					echo "<td>".$result['duree_dc']."</td>";
				}
				if($result['ifr_vfr_id'] == 1){
					echo "<td>".$result['duree_ifr']."</td>";
				}else{
					echo "<td></td>";
				}
			?>
							<td>
								<?php echo $result['arrivee_ifr'] ?>
							</td>
							<td>
								<?php echo $result['poste_long'] ?>
							</td>
							<td>
								<?php echo $result['fonction'] ?>
							</td>
							<td>
								<?php echo $result['observations'] ?>
							</td>
					</tr>
					<?php

}?>
			</tbody>
		</table>
	</div>
