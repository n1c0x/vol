<?php

if (count($_SESSION)==0) {
  /* Echappement de toutes les valeurs entrées dans le formulaire, si elles existent */

  $date = htmlspecialchars($_POST['date'], ENT_QUOTES|ENT_HTML5);

  $cdb_prenom = htmlspecialchars($_POST['cdb_prenom'],ENT_QUOTES|ENT_HTML5);
  $cdb_nom = htmlspecialchars($_POST['cdb_nom'],ENT_QUOTES|ENT_HTML5);
  $opl_prenom = htmlspecialchars($_POST['opl_prenom'],ENT_QUOTES|ENT_HTML5);
  $opl_nom = htmlspecialchars($_POST['opl_nom'],ENT_QUOTES|ENT_HTML5);
  $obs1_prenom = htmlspecialchars($_POST['obs1_prenom'],ENT_QUOTES|ENT_HTML5);
  $obs1_nom = htmlspecialchars($_POST['obs1_nom'],ENT_QUOTES|ENT_HTML5);
  $obs2_prenom = htmlspecialchars($_POST['obs2_prenom'],ENT_QUOTES|ENT_HTML5);
  $obs2_nom = htmlspecialchars($_POST['obs2_nom'],ENT_QUOTES|ENT_HTML5);
  $instructeur_prenom = htmlspecialchars($_POST['instruct_prenom'],ENT_QUOTES|ENT_HTML5);
  $instructeur_nom = htmlspecialchars($_POST['instruct_nom'],ENT_QUOTES|ENT_HTML5);

  $aita_depart = htmlspecialchars($_POST['aita_depart'],ENT_QUOTES|ENT_HTML5);
  $aita_arrivee = htmlspecialchars($_POST['aita_arrivee'],ENT_QUOTES|ENT_HTML5);
  $aita_depart_si_inconnu = htmlspecialchars($_POST['aita_depart_si_inconnu'],ENT_QUOTES|ENT_HTML5);
  $aita_arrivee_si_inconnu = htmlspecialchars($_POST['aita_arrivee_si_inconnu'],ENT_QUOTES|ENT_HTML5);

  if (htmlspecialchars($_POST['hdvjour_heures'],ENT_QUOTES|ENT_HTML5)==""){$hdvjour_heures = 0;}else{$hdvjour_heures = htmlspecialchars($_POST['hdvjour_heures'],ENT_QUOTES|ENT_HTML5);}
  if (htmlspecialchars($_POST['hdvjour_minutes'],ENT_QUOTES|ENT_HTML5)==""){$hdvjour_minutes = 0;}else{$hdvjour_minutes = htmlspecialchars($_POST['hdvjour_minutes'],ENT_QUOTES|ENT_HTML5);}
  if (htmlspecialchars($_POST['hdvnuit_heures'],ENT_QUOTES|ENT_HTML5)==""){$hdvnuit_heures = 0;}else{$hdvnuit_heures = htmlspecialchars($_POST['hdvnuit_heures'],ENT_QUOTES|ENT_HTML5);}
  if (htmlspecialchars($_POST['hdvnuit_minutes'],ENT_QUOTES|ENT_HTML5)==""){$hdvnuit_minutes = 0;}else{$hdvnuit_minutes = htmlspecialchars($_POST['hdvnuit_minutes'],ENT_QUOTES|ENT_HTML5);}

  $arrivee_ifr = htmlspecialchars($_POST['arrifr'],ENT_QUOTES|ENT_HTML5);
  $fonction_id = htmlspecialchars($_POST['fonct'],ENT_QUOTES|ENT_HTML5);
  $poste_id = htmlspecialchars($_POST['poste'],ENT_QUOTES|ENT_HTML5);

  $immat_select = htmlspecialchars($_POST['immat'],ENT_QUOTES|ENT_HTML5);
  $immat_si_inconnu = htmlspecialchars($_POST['immat_si_inconnu'],ENT_QUOTES|ENT_HTML5);
  $type_select = htmlspecialchars($_POST['type_select'],ENT_QUOTES|ENT_HTML5);

  $type = htmlspecialchars($_POST["type"],ENT_QUOTES|ENT_HTML5);
  if ($type == "ifr") {$ifr_vfr_id = 1;}else{$ifr_vfr_id = 2;}

  $observations = htmlspecialchars($_POST['observations'],ENT_QUOTES|ENT_HTML5);
  $simu = FALSE;
  if (isset($_POST['simu'])) {$simu = 1;}
  $dc = FALSE;
  if (isset($_POST['dc'])) {$dc = 1;}

  $user_id = 1;

  $code = array();
  $immat = array();


  $cdb_id = $cdb_prenom." ".$cdb_nom;
  $opl_id = $opl_prenom." ".$opl_nom;
  $obs1_id = $obs1_prenom." ".$obs1_nom;
  $obs2_id = $obs2_prenom." ".$obs2_nom;
  $instructeur_id = $instructeur_prenom." ".$instructeur_nom;


  // addPiloteBDD($con,$cdb_prenom,$cdb_nom);
  // addPiloteBDD($con,$opl_prenom,$opl_nom);
  // addPiloteBDD($con,$obs1_prenom,$obs1_nom);
  // addPiloteBDD($con,$obs2_prenom,$obs2_nom);
  // addPiloteBDD($con,$instruct_prenom,$instruct_nom);

  $date = date("Y-m-d", strtotime($date));


  # Ajout du code Aita dans la BDD s'il n'est pas présent
  $depart_aita_id = addAitaBDD($con,$aita_depart,$aita_depart_si_inconnu);
  $arrivee_aita_id = addAitaBDD($con,$aita_arrivee,$aita_arrivee_si_inconnu);
  # Ajout de l'immatriculation dans la BDD si elle n'est pas présente
  // $immat_id = addImmatBDD($con,$immat_select,$immat_si_inconnu,$type_select);

  $duree_jour = $hdvjour_heures.":".$hdvjour_minutes;
  $duree_nuit = $hdvnuit_heures.":".$hdvnuit_minutes;


  // echo "date: ".$date."<br>";
  // echo "cdb: ".$cdb_id."<br>";
  // echo "opl: ".$opl_id."<br>";
  // echo "obs1: ".$obs1_id."<br>";
  // echo "obs2: ".$obs2_id."<br>";
  // echo "instructeur: ".$instructeur_id."<br>";
  // echo "depart: ".$depart_aita_id."<br>";
  // echo "arrive: ".$arrivee_aita_id."<br>";
  // echo "hdvjour_heures: ".$duree_jour."<br>";
  // echo "hdvjour_minutes: ".$duree_nuit."<br>";
  // echo "arrivee_ifr: ".$arrivee_ifr."<br>";
  // echo "fonction: ".$fonction_id."<br>";
  // echo "poste: ".$poste_id."<br>";
  // echo "immat_id: ".$immat_id."<br>";
  // echo "ifr_vfr_id: ".$ifr_vfr_id."<br>";
  // echo "observations: ".$observations."<br>";
  // echo "simu: ".var_dump($simu)."<br>";
  // echo "double commande: ".var_dump($dc)."<br>";
  // echo "user_id: ".$user_id."<br>";


    $sql = "INSERT INTO vols(date_vol,cdb_id,opl_id,obs1_id,obs2_id,instructeur_id,depart_aita_id,arrivee_aita_id,duree_jour,duree_nuit,arrivee_ifr,fonction_id,poste_id,immat_id,ifr_vfr_id,observations,simu,dc,user_id) VALUES (:date_vol,:cdb_id,:opl_id,:obs1_id,:obs2_id,:instructeur_id,:depart_aita_id,:arrivee_aita_id,:duree_jour,:duree_nuit,:arrivee_ifr,:fonction_id,:poste_id,:ifr_vfr_id,:immat_id,:observations,:simu,:dc,:user_id)";
    $query_prepare = $con->prepare($sql);

  $query_prepare->bindParam(':date_vol', $date, PDO::PARAM_STR);
  $query_prepare->bindParam(':cdb_id', $cdb_id, PDO::PARAM_STR);
  $query_prepare->bindParam(':opl_id', $opl_id, PDO::PARAM_STR);
  $query_prepare->bindParam(':obs1_id', $obs1_id, PDO::PARAM_STR);
  $query_prepare->bindParam(':obs2_id', $obs2_id, PDO::PARAM_STR);
  $query_prepare->bindParam(':instructeur_id', $instructeur_id, PDO::PARAM_STR);
  $query_prepare->bindParam(':depart_aita_id', $depart_aita_id, PDO::PARAM_INT,4);
  $query_prepare->bindParam(':arrivee_aita_id', $arrivee_aita_id, PDO::PARAM_INT,5);
  $query_prepare->bindParam(':duree_jour', $duree_jour, PDO::PARAM_STR);
  $query_prepare->bindParam(':duree_nuit', $duree_nuit, PDO::PARAM_STR);
  $query_prepare->bindParam(':arrivee_ifr', $arrivee_ifr, PDO::PARAM_INT,5);
  $query_prepare->bindParam(':fonction_id', $fonction_id, PDO::PARAM_INT,3);
  $query_prepare->bindParam(':poste_id', $poste_id, PDO::PARAM_INT,3);
  $query_prepare->bindParam(':immat_id', $immat_id, PDO::PARAM_INT,5);
  $query_prepare->bindParam(':ifr_vfr_id', $ifr_vfr_id, PDO::PARAM_INT,5);
  $query_prepare->bindParam(':observations', $observations, PDO::PARAM_STR);
  $query_prepare->bindParam(':simu', $simu, PDO::PARAM_BOOL);
  $query_prepare->bindParam(':dc', $dc, PDO::PARAM_BOOL);
  $query_prepare->bindParam(':user_id', $user_id, PDO::PARAM_INT,16);

  $query_prepare->execute();

}else {
  for ($i=1; $i < count($_SESSION); $i++) {

    /* Echappement de toutes les valeurs entrées dans le formulaire, si elles existent */

    $date = htmlspecialchars($_SESSION["no".$i]['date'], ENT_QUOTES|ENT_HTML5);

    $cdb_prenom = htmlspecialchars($_SESSION["no".$i]['cdb_prenom'],ENT_QUOTES|ENT_HTML5);
    $cdb_nom = htmlspecialchars($_SESSION["no".$i]['cdb_nom'],ENT_QUOTES|ENT_HTML5);
    $opl_prenom = htmlspecialchars($_SESSION["no".$i]['opl_prenom'],ENT_QUOTES|ENT_HTML5);
    $opl_nom = htmlspecialchars($_SESSION["no".$i]['opl_nom'],ENT_QUOTES|ENT_HTML5);
    $obs1_prenom = htmlspecialchars($_SESSION["no".$i]['obs1_prenom'],ENT_QUOTES|ENT_HTML5);
    $obs1_nom = htmlspecialchars($_SESSION["no".$i]['obs1_nom'],ENT_QUOTES|ENT_HTML5);
    $obs2_prenom = htmlspecialchars($_SESSION["no".$i]['obs2_prenom'],ENT_QUOTES|ENT_HTML5);
    $obs2_nom = htmlspecialchars($_SESSION["no".$i]['obs2_nom'],ENT_QUOTES|ENT_HTML5);
    $instructeur_prenom = htmlspecialchars($_SESSION["no".$i]['instruct_prenom'],ENT_QUOTES|ENT_HTML5);
    $instructeur_nom = htmlspecialchars($_SESSION["no".$i]['instruct_nom'],ENT_QUOTES|ENT_HTML5);

    $aita_depart = htmlspecialchars($_SESSION["no".$i]['aita_depart'],ENT_QUOTES|ENT_HTML5);
    $aita_arrivee = htmlspecialchars($_SESSION["no".$i]['aita_arrivee'],ENT_QUOTES|ENT_HTML5);
    // $aita_depart_si_inconnu = htmlspecialchars($_SESSION["no".$i]['aita_depart_si_inconnu'],ENT_QUOTES|ENT_HTML5);
    // $aita_arrivee_si_inconnu = htmlspecialchars($_SESSION["no".$i]['aita_arrivee_si_inconnu'],ENT_QUOTES|ENT_HTML5);

    if (htmlspecialchars($_SESSION["no".$i]['hdvjour_heures'],ENT_QUOTES|ENT_HTML5)==""){$hdvjour_heures = 0;}else{$hdvjour_heures = htmlspecialchars($_SESSION["no".$i]['hdvjour_heures'],ENT_QUOTES|ENT_HTML5);}
    if (htmlspecialchars($_SESSION["no".$i]['hdvjour_minutes'],ENT_QUOTES|ENT_HTML5)==""){$hdvjour_minutes = 0;}else{$hdvjour_minutes = htmlspecialchars($_SESSION["no".$i]['hdvjour_minutes'],ENT_QUOTES|ENT_HTML5);}
    if (htmlspecialchars($_SESSION["no".$i]['hdvnuit_heures'],ENT_QUOTES|ENT_HTML5)==""){$hdvnuit_heures = 0;}else{$hdvnuit_heures = htmlspecialchars($_SESSION["no".$i]['hdvnuit_heures'],ENT_QUOTES|ENT_HTML5);}
    if (htmlspecialchars($_SESSION["no".$i]['hdvnuit_minutes'],ENT_QUOTES|ENT_HTML5)==""){$hdvnuit_minutes = 0;}else{$hdvnuit_minutes = htmlspecialchars($_SESSION["no".$i]['hdvnuit_minutes'],ENT_QUOTES|ENT_HTML5);}

    $arrivee_ifr = htmlspecialchars($_SESSION["no".$i]['arrifr'],ENT_QUOTES|ENT_HTML5);
    $fonction_id = htmlspecialchars($_SESSION["no".$i]['fonction'],ENT_QUOTES|ENT_HTML5);
    $poste_id = htmlspecialchars($_SESSION["no".$i]['poste'],ENT_QUOTES|ENT_HTML5);

    $immat_select = htmlspecialchars($_SESSION["no".$i]['immat'],ENT_QUOTES|ENT_HTML5);
    $immat_si_inconnu = null;
    $type_select = htmlspecialchars($_SESSION["no".$i]['type_select'],ENT_QUOTES|ENT_HTML5);

    $type = htmlspecialchars($_SESSION["no".$i]["type"],ENT_QUOTES|ENT_HTML5);
    $observations = htmlspecialchars($_SESSION["no".$i]['observations'],ENT_QUOTES|ENT_HTML5);
    $simu = FALSE;
    if (isset($_SESSION["no".$i]['simu'])) {$simu = 1;}
    $dc = FALSE;
    if (isset($_SESSION["no".$i]['dc'])) {$dc = 1;}


    $user_id = 1;

    $code = array();
    $immat = array();


    $cdb_id = $cdb_prenom." ".$cdb_nom;
    $opl_id = $opl_prenom." ".$opl_nom;
    $obs1_id = $obs1_prenom." ".$obs1_nom;
    $obs2_id = $obs2_prenom." ".$obs2_nom;
    $instructeur_id = $instructeur_prenom." ".$instructeur_nom;


    // addPiloteBDD($con,$cdb_prenom,$cdb_nom);
    // addPiloteBDD($con,$opl_prenom,$opl_nom);
    // addPiloteBDD($con,$obs1_prenom,$obs1_nom);
    // addPiloteBDD($con,$obs2_prenom,$obs2_nom);
    // addPiloteBDD($con,$instruct_prenom,$instruct_nom);

    $date = date("Y-m-d", strtotime($date));


    # Ajout du code Aita dans la BDD s'il n'est pas présent
    $depart_aita_id = addAitaBDD($con,$aita_depart);
    $arrivee_aita_id = addAitaBDD($con,$aita_arrivee);
    # Ajout de l'immatriculation dans la BDD si elle n'est pas présente

    $immat_id = addImmatBDD($con,$immat_select,$immat_si_inconnu,$type_select);

    $duree_jour = $hdvjour_heures.":".$hdvjour_minutes;
    $duree_nuit = $hdvnuit_heures.":".$hdvnuit_minutes;

    // echo "date: ".$date."<br>";
    // echo "cdb: ".$cdb_id."<br>";
    // echo "opl: ".$opl_id."<br>";
    // echo "obs1: ".$obs1_id."<br>";
    // echo "obs2: ".$obs2_id."<br>";
    // echo "instructeur: ".$instructeur_id."<br>";
    // echo "depart: ".$depart_aita_id."<br>";
    // echo "arrive: ".$arrivee_aita_id."<br>";
    // echo "hdvjour_heures: ".$duree_jour."<br>";
    // echo "hdvjour_minutes: ".$duree_nuit."<br>";
    // echo "arrivee_ifr: ".$arrivee_ifr."<br>";
    // echo "fonction: ".$fonction_id."<br>";
    // echo "ifr_vfr_id: ".$type."<br>";
    // echo "poste: ".$poste_id."<br>";
    // echo "type d'avion char: ".$type_select."<br>";
    // echo "immat_id: ".$immat_id."<br>";
    // echo "observations: ".$observations."<br>";
    // echo "simu: ".var_dump($simu)."<br>";
    // echo "double commande: ".var_dump($dc)."<br>";
    // echo "user_id: ".$user_id."<br>";
    // echo "<hr><hr>";


    $sql = "INSERT INTO vols(date_vol,cdb_id,opl_id,obs1_id,obs2_id,instructeur_id,depart_aita_id,arrivee_aita_id,duree_jour,duree_nuit,arrivee_ifr,fonction_id,poste_id,immat_id,ifr_vfr_id,observations,simu,dc,user_id) VALUES (:date_vol,:cdb_id,:opl_id,:obs1_id,:obs2_id,:instructeur_id,:depart_aita_id,:arrivee_aita_id,:duree_jour,:duree_nuit,:arrivee_ifr,:fonction_id,:poste_id,:immat_id,:ifr_vfr_id,:observations,:simu,:dc,:user_id)";
    $query_prepare = $con->prepare($sql);

    $query_prepare->bindParam(':date_vol', $date, PDO::PARAM_STR);
    $query_prepare->bindParam(':cdb_id', $cdb_id, PDO::PARAM_STR);
    $query_prepare->bindParam(':opl_id', $opl_id, PDO::PARAM_STR);
    $query_prepare->bindParam(':obs1_id', $obs1_id, PDO::PARAM_STR);
    $query_prepare->bindParam(':obs2_id', $obs2_id, PDO::PARAM_STR);
    $query_prepare->bindParam(':instructeur_id', $instructeur_id, PDO::PARAM_STR);
    $query_prepare->bindParam(':depart_aita_id', $depart_aita_id, PDO::PARAM_INT,4);
    $query_prepare->bindParam(':arrivee_aita_id', $arrivee_aita_id, PDO::PARAM_INT,5);
    $query_prepare->bindParam(':duree_jour', $duree_jour, PDO::PARAM_STR);
    $query_prepare->bindParam(':duree_nuit', $duree_nuit, PDO::PARAM_STR);
    $query_prepare->bindParam(':arrivee_ifr', $arrivee_ifr, PDO::PARAM_INT,5);
    $query_prepare->bindParam(':fonction_id', $fonction_id, PDO::PARAM_INT,3);
    $query_prepare->bindParam(':poste_id', $poste_id, PDO::PARAM_INT,3);
    $query_prepare->bindParam(':immat_id', $immat_id, PDO::PARAM_INT,5);
    $query_prepare->bindParam(':ifr_vfr_id', $type, PDO::PARAM_INT,3);
    $query_prepare->bindParam(':observations', $observations, PDO::PARAM_STR);
    $query_prepare->bindParam(':simu', $simu, PDO::PARAM_BOOL);
    $query_prepare->bindParam(':dc', $dc, PDO::PARAM_BOOL);
    $query_prepare->bindParam(':user_id', $user_id, PDO::PARAM_INT,16);

    $query_prepare->execute();
  }
}

if (count($_SESSION)==0) {
  echo "Session vidée<br>";
}else {
  for($i=1;$i<count($_SESSION);$i++)
  {
    session_unset($_SESSION["no".$i]);
  }
  echo "Session remplie<br>";
}

//header('Location:/vol/?page=2');
//exit();

?>
