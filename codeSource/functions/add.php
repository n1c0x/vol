<div class="container">
	<div class="row" id="blue">
		<div class="col-md-12">
			<a href="index.php">
				<img src="img/LogoYvan.png" />
			</a>
		</div>
		<!-- end of span12 -->
	</div>
	<!-- end of row-fluid -->


	<?php

?>
		<h1>Ajouter un vol</h1>
		<hr>

		<form action="index.php?page=4" method="POST" class="row">
			<div class="col-md-6">

				<div class="form-group">
					<label for="datepicker">Date:</label>
					<input type="text" class="form-control" name="date" id="datepicker">
				</div>
				<!-- end of form-group -->

				<div class="form-group">

					<label for="aita_depart">Vol:</label>

					<div class="row">
						<div class="col-md-5">
							<select class="form-control" name='aita_depart' id="aita_depart" onchange='vol_change(1);'>
								<option value="-1"></option>
								<option value="0">N.A.</option>
								<?php
						$query = all_aita($con);
						while($result=$query->fetch(PDO::FETCH_NUM)){
							echo '<option value="'.$result[0].'">'.$result[1].'</option>';
						}
					?>
							</select>
						</div>
						<div class="col-md-2" style="text-align:center;font-size:2rem;">/</div>
						<div class="col-md-5">

							<select class="form-control" name='aita_arrivee' id="aita_arrivee" onchange='vol_change(2);'>
								<option value="-1"></option>
								<option value="0">N.A.</option>
								<?php
						$query = all_aita($con);
						while($result=$query->fetch(PDO::FETCH_NUM)){
							echo '<option value="'.$result[0].'">'.$result[1].'</option>';
						}
					?>
							</select>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row cache">
						<div class="col-md-5">
							<input class="form-control" type="text" name="aita_depart_si_inconnu" id="vol_supp1" maxlength="4" onkeyup='this.value=this.value.toUpperCase()'>
						</div>
						<div class="col-md-2" style="text-align:center;font-size:2rem;">/</div>
						<div class="col-md-5">
							<input class="form-control" type="text" name="aita_arrivee_si_inconnu" id="vol_supp2" maxlength="4" onkeyup='this.value=this.value.toUpperCase()'>
						</div>
					</div>
				</div>

				<!-- end of form-group -->

				<div class="form-group">
					<label for="immat">Immatriculation:</label>

					<select class="form-control" name='immat' id="immat" onchange='immat_change();'>
						<option value="-1"></option>
						<option value="0">N.A.</option>
						<?php
						$query = all_avions($con);
						while($result=$query->fetch(PDO::FETCH_NUM)){
							echo '<option value="'.$result[0].'">'.$result[1].' ('.$result[2].')</option>';
						}
					?>
					</select>
				</div>
				<!-- end of form-group -->

				<!-- Le champ ci-après n'apparait uniquement si la valeur "N.A." est choisie pour l'immatriculation. Dans ce cas, l'immatriculation inscrite dans le champ est sauvegardée dans la base de données -->
			<div id="immat_supp">
				<div class="form-group">
					<div class="row">
						<div class="col-md-4">
							<label for="immat2">Immatriculation:</label>
							<input class="form-control" id="immat2" type="text" name="immat_si_inconnu" maxlength="5" onkeyup='this.value=this.value.toUpperCase()'>
						</div>

						<div class="col-md-4">
							<label for="immat3">Type:</label>
							<select class="form-control" id="immat3" name='type_select'>
								<?php
								for($i=0;$i<count(type_avion($con));$i++){
									echo "<option value=".type_avion($con)[$i][0].">".type_avion($con)[$i][1]."</option>";
								}
							?>
							</select>
						</div>
					</div>
				</div>
			</div>
				<!-- end of form-group -->

				<hr>
				<div class="form-group">
					<h2>Poste occupé:</h2>
					<?php
					$query = all_poste($con);
					while($result=$query->fetch(PDO::FETCH_NUM)){
						echo '
						<div class="radio">
						<label>
							<input type="radio" name="poste" class="poste_select" value="'.$result[0].'" onclick="poste_select()"> '.$result[1].'
						</label>
						</div>
						';
					}
				?>
				</div>
				<!-- end of form-group -->

				<!--
 ######  ########  ########
##    ## ##     ## ##     ##
##       ##     ## ##     ##
##       ##     ## ########
##       ##     ## ##     ##
##    ## ##     ## ##     ##
 ######  ########  ########
 -->


				<hr>
				<div class="poste_hidden_cdb form-group">
					<h3>Commandant de bord</h3>
					<div class="row">
						<div class="col-md-6">
							<label>Prénom:</label>
							<input class="form-control" type="text" placeholder="Votre prénom ici" name="cdb_prenom">
						</div>
						<div class="col-md-6">
							<label>Nom:</label>
							<input class="form-control" type="text" placeholder="Votre nom ici" name="cdb_nom">
						</div>
					</div>
				</div>

				<!--
 #######  ########  ##
##     ## ##     ## ##
##     ## ##     ## ##
##     ## ########  ##
##     ## ##        ##
##     ## ##        ##
 #######  ##        ########
-->
				<hr>
				<div class="poste_hidden_opl form-group">

					<h3>Copilote</h3>
					<div class="row">
						<div class="col-md-6">
							<label>Prénom:</label>
							<input class="form-control" type="text" name="opl_prenom">
						</div>
						<div class="col-md-6">
							<label>Nom:</label>
							<input class="form-control" type="text" name="opl_nom">
						</div>

					</div>
				</div>

				<!--
 #######  ########   ######
##     ## ##     ## ##    ##
##     ## ##     ## ##
##     ## ########   ######
##     ## ##     ##       ##
##     ## ##     ## ##    ##
 #######  ########   ######
 -->
				<hr>
				<div class="form-group">
					<h2>Observateur(s) présent(s):</h2>


					<div class="radio">
						<label>
							<input type="radio" name="nb_obs" value="nb_obs0" class="obs_select" onclick='obs_select()'> 0
						</label>
					</div>

					<div class="radio">

						<label>
							<input type="radio" name="nb_obs" value="nb_obs1" class="obs_select" onclick='obs_select()'> 1
						</label>
					</div>
					<div class="radio">

						<label>
							<input type="radio" name="nb_obs" value="nb_obs2" class="obs_select" onclick='obs_select()'> 2
						</label>
					</div>
				</div>
				<div class="obs_hidden form-group">
					<h3>Observateur 1</h3>
					<label>Prénom:</label>
					<input class="form-control" type="text" name="obs1_prenom">
					<label>Nom:</label>
					<input class="form-control" type="text" name="obs1_nom">
				</div>

				<div class="obs_hidden2 form-group">
					<h3>Observateur 2</h3>
					<label>Prénom:</label>
					<input class="form-control" type="text" name="obs2_prenom">
					<label>Nom:</label>
					<input class="form-control" type="text" name="obs2_nom">
				</div>

				<!--
#### ##    ##  ######  ######## ########  ##     ##  ######  ######## ######## ##     ## ########
 ##  ###   ## ##    ##    ##    ##     ## ##     ## ##    ##    ##    ##       ##     ## ##     ##
 ##  ####  ## ##          ##    ##     ## ##     ## ##          ##    ##       ##     ## ##     ##
 ##  ## ## ##  ######     ##    ########  ##     ## ##          ##    ######   ##     ## ########
 ##  ##  ####       ##    ##    ##   ##   ##     ## ##          ##    ##       ##     ## ##   ##
 ##  ##   ### ##    ##    ##    ##    ##  ##     ## ##    ##    ##    ##       ##     ## ##    ##
#### ##    ##  ######     ##    ##     ##  #######   ######     ##    ########  #######  ##     ##
 -->
				<hr>
				<div class="form-group">
					<h2>Instructeur présent:</h2>


					<div class="radio">
						<label>
							<input type="radio" name="inst_bool" value="inst_yes" class="inst" onclick='inst_select()'> Oui
						</label>
					</div>

					<div class="radio">
						<label>
							<input type="radio" name="inst_bool" value="inst_no" class="inst" onclick='inst_select()'> Non
						</label>
					</div>

				</div>

				<div class="inst_hidden form-group">
					<h3>Instructeur</h3>
					<label>Prénom:</label>
					<input class="form-control" type="text" name="instruct_prenom">
					<label>Nom:</label>
					<input class="form-control" type="text" name="instruct_nom">
				</div>
				<!-- end of col-md-6 -->

			</div>


			<div class="col-md-6">
				<h2>Heures de vol</h2>
				<div class="form-group">
					<h3>Jour:</h3>
					<div class="row">
						<div class="col-md-5">
							<input class="form-control" type="number" name="hdvjour_heures" min="0" max="23">
						</div>
						<div class="col-md-1">H</div>
						<div class="col-md-5">
							<input class="form-control" type="number" name="hdvjour_minutes" min="0" max="59">
						</div>
						<div class="col-md-1">Min</div>
					</div>


					<div class="form-group">

						<h3>Nuit:</h3>
						<div class="row">
							<div class="col-md-5">
								<input class="form-control" type="number" name="hdvnuit_heures" min="0" max="23">
							</div>
							<div class="col-md-1">H</div>
							<div class="col-md-5">
								<input class="form-control" type="number" name="hdvnuit_minutes" min="0" max="59">
							</div>
							<div class="col-md-1">Min</div>
						</div>


					</div>
					<hr>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="simu" value="simu">Simu
							</label>
						</div>
					</div>

					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="dc" value="dc">DC
							</label>
						</div>
					</div>

					<div class="form-group">
						<label>Arr IFR:</label>
						<input type="text" name="arrifr" class="form-control" maxlength="2" value="1">
					</div>

					<hr>
					<div class="form-group">
						<h2>Fonction:</h2>

						<?php
				$query = all_fonction($con);
				while($result=$query->fetch(PDO::FETCH_NUM)){
					echo '
					<div class="radio">
					<label class="radio">
						<input type="radio" name="fonct" value="'.$result[0].'"> '.$result[1].'
					</label>
					</div>
					';
				}
			?>
					</div>
					<hr>
					<div class="form-group">
						<h2>Type:</h2>


						<div class="radio">
							<label>
								<input type="radio" name="type" value="ifr"> IFR
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="type" value="vfr"> VFR
							</label>
						</div>
					</div>
				</div>
				<hr>
				<div class="form-group">
					<h2>Observations:</h2>

					<textarea class="form-control" rows="4" name="observations"></textarea>
				</div>

				<button type="reset" class="btn">Reset</button>
				<button type="submit" class="btn btn-info">Sauvegarder</button>
		</form>
