<h1>Lecture du fichier CSV importé</h1>
<?php

$filename = $_FILES['userfile']['tmp_name'];

$sql = "INSERT INTO vol.vols
				VALUES (
					:id_vol,
					:date_vol,
					:cdb_id,
					:opl_id,
					:obs1_id,
					:obs2_id,
					:instructeur_id,
					:depart_aita_id,
					:arrivee_aita_id,
					:duree_jour,
					:duree_nuit,
					:arrivee_ifr,
					:fonction_id,
					:poste_id,
					:immat_id,
					:ifr_vfr_id,
					:observations,
					:simu,
					:dc,
					:user_id
					)";

if (($handle = fopen($filename, 'r')) !== FALSE) {
	$id=1;
	while (($data = fgetcsv($handle,0,';')) !== FALSE) {
		foreach ($data as $key => $value) {
			$data[$key] = htmlspecialchars($value,ENT_QUOTES|ENT_HTML5);
		}
		reset($data);
		if(preg_match("#/#",$data[0])){
			echo "<pre>";
			print_r($data);
			echo "</pre>";

			echo "ID=".$id."<br>";
			## Date ##
			echo "Date (transformer en timestamp): ".$data[0]."<br>";
			$date = date_create_from_format('d/m/Y',$data[0]);
			$date_vol = date_format($date,'Y-m-d');
			echo "Date à sauvegarder: ".$date_vol."<br>";

			## CDB ##
			echo "CDB (diviser en prénom et nom): ";
			if($data[1] !== ""){
				$poste_id = 2;
				$prenom_nom_cdb = explode(" ",$data[1]);
				echo "nom cdb".$prenom_nom_cdb[0];
				echo "prénom cdb".$prenom_nom_cdb[1];
			}
			else{
				$poste_id = 1;
				$prenom_nom_cdb[0] = "";
				$prenom_nom_cdb[1] = "";
			}
			echo "<br>";

			## OPL ##
			echo "OPL (diviser en prénom et nom): ";
			if($data[2] !== ""){
				$prenom_nom_opl = explode(" ",$data[2]);
				echo "nom opl ".$prenom_nom_opl[0]." ";
				echo "prénom opl ".$prenom_nom_opl[1];
			}
						else{
				$prenom_nom_opl[0] = "";
				$prenom_nom_opl[1] = "";
			}
			echo "<br>";

			## OBS1 ##
			echo "OBS1 (diviser en prénom et nom): ";
			if($data[3] !== ""){
				$prenom_nom_obs1 = explode(" ",$data[3]);
				echo "nom observateur 1 ".$prenom_nom_obs1[0]." ";
				echo "prénom observateur 1 ".$prenom_nom_obs1[1];
			}
			else{
				$prenom_nom_obs1[0] = "";
				$prenom_nom_obs1[1] = "";
			}
			echo "<br>";

			## OBS2 ##
			echo "OBS2 (diviser en prénom et nom): ";
			if($data[4] !== ""){
				$prenom_nom_obs2 = explode(" ",$data[4]);
				echo "nom observateur 2 ".$prenom_nom_obs2[0]." ";
				echo "prénom observateur 2 ".$prenom_nom_obs2[1];
			}
			else{
				$prenom_nom_obs2[0] = "";
				$prenom_nom_obs2[1] = "";
			}
			echo "<br>";

			## Instructeur ##
			echo "INSTRUCTEUR (diviser en prénom et nom): ";
			if($data[5] !== ""){
				$prenom_nom_inst = explode(" ",$data[5]);
				echo "nom instructeur ".$prenom_nom_inst[0]." ";
				echo "prénom instructeur ".$prenom_nom_inst[1];
			}
			else{
				$prenom_nom_inst[0] = "";
				$prenom_nom_inst[1] = "";
			}
			echo "<br>";

			## Départ/Arrivée ##
			echo "Vol (diviser en départ et arrivée):";
			$dep_dest = array();
			if(preg_match("#/#", $data[6])){
				$dep_dest = explode("/",$data[6]);
				echo "depart ".$dep_dest[0]." ";
				echo "arrivée ".$dep_dest[1];
			}
			else{
				$dep_dest[0] = null;
				$dep_dest[1] = null;
				echo "Voir observation ";
			}
			echo "<br>";

			## Durée jour ##
			echo "HDV jour (diviser en heures et minutes): ";
			if($data[7] !== ""){
				$hdv_jour = explode(":",$data[7]);
				echo $hdv_jour[0]." heures ";
				echo $hdv_jour[1]." minutes";
			}
			else{
				$hdv_jour[0] = "";
				$hdv_jour[1] = "";
			}
			echo "<br>";

			## Durée nuit ##
			echo "HDV nuit (diviser en heures et minutes): ";
			if($data[8] !== ""){
				$hdv_nuit = explode(":",$data[8]);
				echo $hdv_nuit[0]." heures ";
				echo $hdv_nuit[1]." minutes";
			}
			else{
				$hdv_nuit[0] = "";
				$hdv_nuit[1] = "";
			}
			echo "<br>";

			## Vol IFR ##
			echo "Vol IFR = Vol jour + Vol nuit: ";
			echo intval(($hdv_jour[1] + $hdv_nuit[1])/60)+$hdv_jour[0] + $hdv_nuit[0].":";
			echo ($hdv_jour[1] + $hdv_nuit[1])%60;
			echo "<br>";

			$arrivee_ifr = $data[17];
			if ($data[18] == "PF") {
				$fonction = 1;
			} else if ($data[18] == "PNF") {
				$fonction = 2;
			}else{
				$fonction = 3;
			}

			$immatriculation = $data[19];
			$simu = 0;
			if (strpbrk($immatriculation,"simu")) {$simu = 1;}
			$type_avion = $data[20];

			$obs = $data[21];
			echo "Arrivée IFR: ".$arrivee_ifr."<br>";
			if ($arrivee_ifr != null) {$type = "ifr";}else{$type = "vfr";}
			if ($type == "ifr") {$type = 1;}else{$type = 2;}


			echo "Fonction: ".$fonction."<br>";
			echo "Immatriculation: ".$immatriculation."<br>";
			echo "Type d'avion: ".$type_avion."<br>";
			echo "Type de vol: ".$type."<br>";
			echo "Observations: ".$obs."<br>";
			echo "Simu: ".$simu."<br>";
			$dc = 0;
			echo "DC: ".$dc."<br>";

			$send_to_formular = array();
			$send_to_formular['date'] = $date_vol;
			$send_to_formular['cdb_prenom'] = $prenom_nom_cdb[0];
			$send_to_formular['cdb_nom'] = $prenom_nom_cdb[1];
			$send_to_formular['opl_prenom'] = $prenom_nom_opl[0];
			$send_to_formular['opl_nom'] = $prenom_nom_opl[1];
			$send_to_formular['obs1_prenom'] = $prenom_nom_obs1[0];
			$send_to_formular['obs1_nom'] = $prenom_nom_obs1[1];
			$send_to_formular['obs2_prenom'] = $prenom_nom_obs2[0];
			$send_to_formular['obs2_nom'] = $prenom_nom_obs2[1];
			$send_to_formular['instruct_prenom'] = $prenom_nom_inst[0];
			$send_to_formular['instruct_nom'] = $prenom_nom_inst[1];
			$send_to_formular['aita_depart'] = $dep_dest[0];
			$send_to_formular['aita_arrivee'] = $dep_dest[1];
			$send_to_formular['hdvjour_heures'] = $hdv_jour[0];
			$send_to_formular['hdvjour_minutes'] = $hdv_jour[1];
			$send_to_formular['hdvnuit_heures'] = $hdv_nuit[0];
			$send_to_formular['hdvnuit_minutes'] = $hdv_nuit[1];
			$send_to_formular['arrifr'] = $arrivee_ifr;
			$send_to_formular['fonction'] = $fonction;
			$send_to_formular['poste'] = $poste_id;
			$send_to_formular['type_select'] = $type_avion;
			$send_to_formular['immat'] = $immatriculation;
			$send_to_formular['type'] = $type;
			$send_to_formular['observations'] = $obs;
			$send_to_formular['simu'] = $simu;
			$send_to_formular['dc'] = $dc;

			$_SESSION["no".$id] = $send_to_formular;
			echo "<br>";

			$id++;

		# Ajouter Pilote dans la BDD s'il n'y existe pas encore
		# Ajouter Depart dans la BDD s'il n'y existe pas encore
		# Ajouter Arrivee dans la BDD s'il n'y existe pas encore
		# Ajouter Poste dans la BDD s'il n'y existe pas encore
		# Ajouter Fonction dans la BDD s'il n'y existe pas encore
		# Ajouter Immatriculation (et donc type) dans la BDD s'il n'y existe pas encore

		echo "<div class='separateur'></div>";
		}
	}
	fclose($handle);
}
else{
	echo "File not opened";
}

echo "<pre>";
print_r($_SESSION);
echo "</pre>";

?>

</form>
