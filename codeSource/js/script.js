$(function() {
  $( "#datepicker" ).datepicker();
	$( "#datepicker" ).datepicker({
		dateFormat: "mm-dd-yy"
	});
	$( "#datepicker" ).datepicker( "option", "dateFormat","dd-mm-yy");
  });

function immat_change() {
	if($('#immat').val() == 0) {
		$('#immat_supp').css("display","inline");
	}
	else{
		$('#immat_supp').css("display","none");
	}
}

function vol_change(id){
	if($('#aita_depart').val() == 0) {
		$('.cache').css("display","block");
		$('#vol_supp1').css("display","inline");

		if($('#aita_arrivee').val() == 0) {
			$('#vol_supp2').css("display","inline");
		}
		else{
			$('#vol_supp2').css("display","none");
		}
	}
	else{
		$('#vol_supp1').css("display","none");

		if($('#aita_arrivee').val() == 0) {
			$('#vol_supp2').css("display","inline");
			$('.cache').css("display","block");
		}
		else{
			$('#vol_supp2').css("display","none");
			$('.cache').css("display","none");
		}
	}
}

$(".poste_select").click(poste_select());
function poste_select(){
	if($("input[type=radio][name='poste']:checked").val() == '1'){
		$('#poste_hidden_cdb').css("display","inline");
		$('.poste_hidden_opl').css("display","block");
		$('.poste_hidden_cdb').css("display","none");
	}else if($("input[type=radio][name='poste']:checked").val() == '2'){
		$('#poste_hidden_opl').css("display","inline");
		$('.poste_hidden_cdb').css("display","block");
		$('.poste_hidden_opl').css("display","none");
	}
}

$(".obs_select").click(obs_select());
function obs_select(){
	if($("input[type=radio][name='nb_obs']:checked").val() == 'nb_obs0'){
		$('#obs_hidden').css("display","inline");
		$('.obs_hidden').css("display","none");
		$('.obs_hidden2').css("display","none");
	}else if($("input[type=radio][name='nb_obs']:checked").val() == 'nb_obs1'){
		$('#obs_hidden').css("display","inline");
		$('.obs_hidden').css("display","block");
		$('.obs_hidden2').css("display","none");
	}else if($("input[type=radio][name='nb_obs']:checked").val() == 'nb_obs2'){
		$('#obs_hidden').css("display","inline");
		$('.obs_hidden').css("display","block");
		$('.obs_hidden2').css("display","block");
	}
}

$(".inst_select").click(inst_select());
function inst_select(){
	if($("input[type=radio][name='inst_bool']:checked").val() == 'inst_no'){
		$('#inst_hidden').css("display","inline");
		$('.inst_hidden').css("display","none");
	}else if($("input[type=radio][name='inst_bool']:checked").val() == 'inst_yes'){
		$('#inst_hidden').css("display","inline");
		$('.inst_hidden').css("display","block");
	}
}

$(document).ready(function() {
    $('#tableau_vols').DataTable({
      "language": {
            "url":"lang/French.json",
            // "url":"lang/German.json"
            // "url":"lang/English.json"
            "decimal": ",",
            "thousands": " "
          }
    });
} );
