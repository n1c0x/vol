#!/bin/sh
if [[ $UID -ge 1000 ]]; then    # User normal
	echo "Veuillez lancer le programme en tant que root"
elif [[ $UID -eq 0 ]]; then     # user root
	./saveSQL/export.sh
	systemctl stop mysqld
	systemctl status -l mysqld
	systemctl stop httpd
	systemctl status -l httpd
fi
