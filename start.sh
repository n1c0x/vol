#!/bin/sh
if [[ $UID -ge 1000 ]]; then    # User normal
	echo "Veuillez lancer le programme en tant que root"
elif [[ $UID -eq 0 ]]; then     # user root
	systemctl start mysqld
	systemctl status -l mysqld
	systemctl start httpd
	systemctl status -l httpd
fi
