-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 18 Août 2014 à 08:49
-- Version du serveur :  5.6.14
-- Version de PHP :  5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `vol`
--

-- --------------------------------------------------------

--
-- Structure de la table `aita`
--

CREATE TABLE IF NOT EXISTS `aita` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `code_aita` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `aita`
--

INSERT INTO `aita` (`id`, `code_aita`) VALUES
(1, 'CDG'),
(2, 'SXB'),
(3, 'BDX');

-- --------------------------------------------------------

--
-- Structure de la table `hdv_par_avion`
--

CREATE TABLE IF NOT EXISTS `hdv_par_avion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `cdb_jour` varchar(11) NOT NULL,
  `cdb_nuit` varchar(11) NOT NULL,
  `opl_jour` varchar(11) NOT NULL,
  `opl_nuit` varchar(11) NOT NULL,
  `dc_jour` varchar(11) NOT NULL,
  `dc_nuit` varchar(11) NOT NULL,
  `inst_jour` varchar(11) NOT NULL,
  `inst_nuit` varchar(11) NOT NULL,
  `simu` varchar(11) NOT NULL,
  `ifr` varchar(11) NOT NULL,
  `arri_ifr` varchar(11) NOT NULL,
  `total` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Contenu de la table `hdv_par_avion`
--

INSERT INTO `hdv_par_avion` (`id`, `type`, `cdb_jour`, `cdb_nuit`, `opl_jour`, `opl_nuit`, `dc_jour`, `dc_nuit`, `inst_jour`, `inst_nuit`, `simu`, `ifr`, `arri_ifr`, `total`) VALUES
(2, 'A340', '0:00', '0:00', '914:04', '803:53', '0:00', '0:00', '0:00', '0:00', '61:00', '1746:57', '227', '1717:57'),
(3, 'A341', '0:00', '0:00', '845:38', '752:57', '0:00', '0:00', '0:00', '0:00', '47:30', '1637:23', '229', '1598:35'),
(4, 'A342', '118:42', '34:41', '0:00', '0:00', '0:00', '0:00', '0:00', '0:00', '0:00', '153:23', '94', '153:23'),
(5, 'A318', '', '', '', '', '', '', '', '', '', '', '', ''),
(6, 'A319', '', '', '', '', '', '', '', '', '', '', '', ''),
(7, 'A320', '', '', '', '', '', '', '', '', '', '', '', ''),
(8, 'A321', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, 'AJET', '657:50', '51:40', '75:05', '4:25', '64:15', '7:00', '613:05', '0:00', '60:15', '19:35', '146', '860:15'),
(10, 'B737', '0:00', '0:00', '0:00', '0:00', '0:00', '0:00', '0:00', '0:00', '60:00', '0:00', '0', '0:00'),
(11, 'BE55', '0:00', '0:00', '0:00', '0:00', '42:45', '0:00', '0:00', '0:00', '30:00', '37:30', '62', '42:45'),
(12, 'CM170', '1043:15', '65:05', '187:20', '7:40', '96:55', '7:40', '884:30', '0:00', '62:30', '115:55', '181', '1407:55'),
(13, 'M20', '1:30', '0:00', '31:15', '2:25', '0:00', '0:00', '0:00', '0:00', '0:00', '0:00', '4', '35:10'),
(15, 'C152', '47:20', '0:00', '0:00', '0:00', '0:00', '0:00', '47:00', '0:00', '0:00', '0:00', '0', '47:20'),
(16, 'C172', '23:25', '0:00', '0:00', '0:00', '0:00', '0:00', '18:00', '0:00', '0:00', '0:00', '0', '23:25'),
(17, 'C177', '0:40', '0:00', '0:00', '0:00', '3:45', '0:00', '0:00', '0:00', '0:00', '0:00', '0', '4:25'),
(18, 'CAP10', '4:45', '0:00', '0:00', '0:00', '44:20', '0:00', '0:55', '0:00', '0:00', '0:00', '0', '49:05'),
(19, 'CO02', '10:05', '0:00', '0:00', '0:00', '0:30', '0:00', '10:05', '0:00', '0:00', '0:00', '0', '10:35'),
(20, 'divers', '21:50', '0:00', '0:00', '0:00', '1:40', '0:00', '20:35', '0:00', '0:00', '0:00', '0', '23:30'),
(21, 'DR360', '4:35', '0:00', '0:00', '0:00', '0:20', '0:00', '0:00', '0:00', '0:00', '0:00', '0', '4:55'),
(22, 'DR400', '190:30', '0:00', '0:00', '0:00', '2:25', '0:00', '161:50', '0:00', '0:00', '0:00', '0', '192:55'),
(23, 'F1CR', '917:30', '65:10', '0:00', '0:00', '31:35', '2:00', '0:00', '0:00', '82:00', '26:00', '167', '1016:15'),
(24, 'F1CT', '747:55', '38:10', '0:00', '0:00', '4:30', '0:00', '0:00', '0:00', '23:00', '2:30', '79', '790:35'),
(25, 'HR200', '51:30', '0:00', '0:00', '0:00', '48:10', '0:00', '10:30', '0:00', '', '0:00', '0', '99:40'),
(26, 'J3', '27:25', '0:00', '0:00', '0:00', '0:40', '0:00', '25:15', '0:00', '0:00', '0:00', '0', '28:05'),
(27, 'M201', '1:00', '0:00', '0:00', '0:00', '0:00', '0:00', '1:00', '0:00', '0:00', '0:00', '0', '1:00'),
(28, 'M893', '1:55', '0:00', '0:00', '0:00', '0:00', '0:00', '1:55', '0:00', '0:00', '0:00', '0', '1:55'),
(29, 'MS880', '259:00', '0:00', '0:00', '0:00', '20:35', '0:00', '201:10', '0:00', '0:00', '0:00', '0', '279:35'),
(30, 'R1180', '0:00', '0:00', '0:00', '0:00', '6:20', '0:00', '0:00', '0:00', '0:00', '0:00', '0', '6:20'),
(31, 'R2100', '1:05', '0:00', '0:00', '0:00', '0:00', '0:00', '0:00', '0:00', '0:00', '0:00', '0', '1:05'),
(32, 'TB10', '7:35', '0:00', '0:00', '0:00', '2:25', '0:00', '0:00', '0:00', '0:00', '0:00', '0', '10:00');

-- --------------------------------------------------------

--
-- Structure de la table `immat_type`
--

CREATE TABLE IF NOT EXISTS `immat_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `immatriculation` varchar(8) NOT NULL,
  `type` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=183 ;

--
-- Contenu de la table `immat_type`
--

INSERT INTO `immat_type` (`id`, `immatriculation`, `type`) VALUES
(1, 'FGFKA', 3),
(2, 'FGFKB', 3),
(3, 'FGFKD', 3),
(4, 'FGFKE', 3),
(5, 'FGFKF', 3),
(6, 'FGFKG', 3),
(7, 'FGFKH', 3),
(8, 'FGFKI', 3),
(9, 'FGFKJ', 3),
(10, 'FGFKJ', 3),
(11, 'FGFKK', 3),
(12, 'FGFKL', 3),
(13, 'FGFKM', 3),
(14, 'FGFKN', 3),
(15, 'FGFKO', 3),
(16, 'FGFKP', 3),
(17, 'FGFKQ', 3),
(18, 'FGFKR', 3),
(19, 'FGFKS', 3),
(20, 'FGFKT', 3),
(21, 'FGFKU', 3),
(22, 'FGFKV', 3),
(23, 'FGFKX', 3),
(24, 'FGFKY', 3),
(25, 'FGFKZ', 3),
(26, 'FGGEA', 3),
(27, 'FGGEB', 3),
(28, 'FGGEC', 3),
(29, 'FGGEE', 3),
(30, 'FGGEF', 3),
(31, 'FGGEG', 3),
(32, 'FGHQA', 3),
(33, 'FGHQB', 3),
(34, 'FGHQC', 3),
(35, 'FGHQD', 3),
(36, 'FGHQE', 3),
(37, 'FGHQF', 3),
(38, 'FGHQG', 3),
(39, 'FGHQH', 3),
(40, 'FGHQI', 3),
(41, 'FGHQJ', 3),
(42, 'FGHQK', 3),
(43, 'FGHQL', 3),
(44, 'FGHQM', 3),
(45, 'FGHQO', 3),
(46, 'FGHQP', 3),
(47, 'FGHQQ', 3),
(48, 'FGHQR', 3),
(49, 'FGJVA', 3),
(50, 'FGJVB', 3),
(51, 'FGJVG', 3),
(52, 'FGJVW', 3),
(53, 'FGKXA', 3),
(54, 'FGKXB', 3),
(55, 'FGKXC', 3),
(56, 'FGKXD', 3),
(57, 'FGKXE', 3),
(58, 'FGKXF', 3),
(59, 'FGKXG', 3),
(60, 'FGKXH', 3),
(61, 'FGKXI', 3),
(62, 'FGKXJ', 3),
(63, 'FGKXK', 3),
(64, 'FGKXL', 3),
(65, 'FGKXM', 3),
(66, 'FGKXN', 3),
(67, 'FGKXO', 3),
(68, 'FGKXP', 3),
(69, 'FGKXQ', 3),
(70, 'FGKXR', 3),
(71, 'FGKXS', 3),
(72, 'FGKXT', 3),
(73, 'FGKXU', 3),
(74, 'FGKXV', 3),
(75, 'FGKXY', 3),
(76, 'FGKXZ', 3),
(77, 'FGLGG', 3),
(78, 'FGLGH', 3),
(79, 'FGLGM', 3),
(80, 'FGMZA', 4),
(81, 'FGMZB', 4),
(82, 'FGMZC', 4),
(83, 'FGMZD', 4),
(84, 'FGMZE', 4),
(85, 'FGPMA', 2),
(86, 'FGPMB', 2),
(87, 'FGPMC', 2),
(88, 'FGPMD', 2),
(89, 'FGPME', 2),
(90, 'FGPMF', 2),
(91, 'FGPMG', 2),
(92, 'FGRHA', 2),
(93, 'FGRHB', 2),
(94, 'FGRHC', 2),
(95, 'FGRHD', 2),
(96, 'FGRHE', 2),
(97, 'FGRHF', 2),
(98, 'FGRHG', 2),
(99, 'FGRHH', 2),
(100, 'FGRHI', 2),
(101, 'FGRHJ', 2),
(102, 'FGRHK', 2),
(103, 'FGRHL', 2),
(104, 'FGRHM', 2),
(105, 'FGRHN', 2),
(106, 'FGRHO', 2),
(107, 'FGRHP', 2),
(108, 'FGRHQ', 2),
(109, 'FGRHR', 2),
(110, 'FGRHS', 2),
(111, 'FGRHT', 2),
(112, 'FGRHU', 2),
(113, 'FGRHV', 2),
(114, 'FGRHX', 2),
(115, 'FGRHY', 2),
(116, 'FGRHZ', 2),
(117, 'FGRXA', 2),
(118, 'FGRXB', 2),
(119, 'FGRXC', 2),
(120, 'FGRXD', 2),
(121, 'FGRXE', 2),
(122, 'FGRXF', 2),
(123, 'FGRXG', 2),
(124, 'FGRXH', 2),
(125, 'FGRXI', 2),
(126, 'FGRXJ', 2),
(127, 'FGRXK', 2),
(128, 'FGRXL', 2),
(129, 'FGRXM', 2),
(130, 'FGRXN', 2),
(131, 'FGRXR', 2),
(132, 'FGTAD', 4),
(133, 'FGTAE', 4),
(134, 'FGTAH', 4),
(135, 'FGTAI', 4),
(136, 'FGTAJ', 4),
(137, 'FGTAK', 4),
(138, 'FGTAL', 4),
(139, 'FGTAM', 4),
(140, 'FGTAN', 4),
(141, 'FGTAO', 4),
(142, 'FGTAP', 4),
(143, 'FGTAQ', 4),
(144, 'FGTAR', 4),
(145, 'FGTAS', 4),
(146, 'FGTAT', 4),
(147, 'FGTAU', 4),
(148, 'FGTAV', 4),
(149, 'FGTAX', 4),
(150, 'FGTAY', 4),
(151, 'FGTAZ', 4),
(152, 'FGUGA', 1),
(153, 'FGUGB', 1),
(154, 'FGUGC', 1),
(155, 'FGUGD', 1),
(156, 'FGUGE', 1),
(157, 'FGUGF', 1),
(158, 'FGUGG', 1),
(159, 'FGUGH', 1),
(160, 'FGUGI', 1),
(161, 'FGUGJ', 1),
(162, 'FGUGK', 1),
(163, 'FGUGL', 1),
(164, 'FGUGM', 1),
(165, 'FGUGN', 1),
(166, 'FGUGO', 1),
(167, 'FGUGP', 1),
(168, 'FGUGQ', 1),
(169, 'FGUGR', 1),
(170, 'FHBNA', 3),
(171, 'FHBNC', 3),
(172, 'FHBND', 3),
(173, 'FHBNG', 3),
(174, 'FHEPA', 3),
(175, 'FHEPB', 3),
(176, 'FHEPC', 3),
(177, 'FHEPD', 3),
(178, 'FHEPE', 3),
(179, 'simuA318', 1),
(180, 'simuA319', 2),
(181, 'simuA320', 3),
(182, 'simuA321', 4);

-- --------------------------------------------------------

--
-- Structure de la table `nb_moteurs_avion`
--

CREATE TABLE IF NOT EXISTS `nb_moteurs_avion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nb_moteurs_avion` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nb_moteurs_avion` (`nb_moteurs_avion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `nb_moteurs_avion`
--

INSERT INTO `nb_moteurs_avion` (`id`, `nb_moteurs_avion`) VALUES
(1, 'Monomoteur'),
(2, 'Multimoteur');

-- --------------------------------------------------------

--
-- Structure de la table `pilote`
--

CREATE TABLE IF NOT EXISTS `pilote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom-pilote` varchar(255) NOT NULL,
  `nom_pilote` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `recap`
--

CREATE TABLE IF NOT EXISTS `recap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_avion` int(11) DEFAULT NULL,
  `cdb_jour_heures` int(11) NOT NULL,
  `cdb_jour_minutes` int(11) NOT NULL,
  `cdb_nuit_heures` int(11) NOT NULL,
  `cdb_nuit_minutes` int(11) NOT NULL,
  `opl_jour_heures` int(11) NOT NULL,
  `opl_jour_minutes` int(11) NOT NULL,
  `opl_nuit_heures` int(11) NOT NULL,
  `opl_nuit_minutes` int(11) NOT NULL,
  `dc_jour_heures` int(11) NOT NULL,
  `dc_jour_minutes` int(11) NOT NULL,
  `dc_nuit_heures` int(11) NOT NULL,
  `dc_nuit_minutes` int(11) NOT NULL,
  `instruct_jour_heures` int(11) NOT NULL,
  `instruct_jour_minutes` int(11) NOT NULL,
  `instruct_nuit_heures` int(11) NOT NULL,
  `instruct_nuit_minutes` int(11) NOT NULL,
  `simu_heures` int(11) NOT NULL,
  `simu_minutes` int(11) NOT NULL,
  `ifr_heures` int(11) NOT NULL,
  `ifr_minutes` int(11) NOT NULL,
  `arrivees_ifr` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_avion` (`type_avion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Contenu de la table `recap`
--

INSERT INTO `recap` (`id`, `type_avion`, `cdb_jour_heures`, `cdb_jour_minutes`, `cdb_nuit_heures`, `cdb_nuit_minutes`, `opl_jour_heures`, `opl_jour_minutes`, `opl_nuit_heures`, `opl_nuit_minutes`, `dc_jour_heures`, `dc_jour_minutes`, `dc_nuit_heures`, `dc_nuit_minutes`, `instruct_jour_heures`, `instruct_jour_minutes`, `instruct_nuit_heures`, `instruct_nuit_minutes`, `simu_heures`, `simu_minutes`, `ifr_heures`, `ifr_minutes`, `arrivees_ifr`) VALUES
(1, 5, 0, 0, 0, 0, 914, 4, 803, 53, 0, 0, 0, 0, 0, 0, 0, 0, 61, 0, 1746, 57, 227),
(2, 6, 0, 0, 0, 0, 845, 38, 752, 57, 0, 0, 0, 0, 0, 0, 0, 0, 47, 30, 1637, 23, 229),
(3, 7, 118, 42, 34, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 153, 23, 94),
(4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 8, 657, 50, 51, 40, 75, 5, 4, 25, 64, 15, 7, 0, 613, 5, 0, 0, 60, 15, 19, 35, 146),
(9, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60, 0, 0, 0, 0),
(10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 42, 45, 0, 0, 0, 0, 0, 0, 30, 0, 37, 30, 62),
(11, 11, 1043, 15, 65, 5, 187, 20, 7, 40, 96, 55, 7, 40, 884, 30, 0, 0, 62, 30, 115, 55, 181),
(12, 12, 1, 30, 0, 0, 31, 15, 2, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4),
(13, 13, 47, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 14, 23, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 15, 0, 40, 0, 0, 0, 0, 0, 0, 3, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(16, 16, 4, 45, 0, 0, 0, 0, 0, 0, 44, 20, 0, 0, 0, 55, 0, 0, 0, 0, 0, 0, 0),
(17, 17, 10, 5, 0, 0, 0, 0, 0, 0, 0, 30, 0, 0, 10, 5, 0, 0, 0, 0, 0, 0, 0),
(18, 18, 21, 50, 0, 0, 0, 0, 0, 0, 1, 40, 0, 0, 20, 35, 0, 0, 0, 0, 0, 0, 0),
(19, 19, 4, 35, 0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 20, 190, 30, 0, 0, 0, 0, 0, 0, 2, 25, 0, 0, 161, 50, 0, 0, 0, 0, 0, 0, 0),
(21, 21, 917, 30, 65, 10, 0, 0, 0, 0, 31, 35, 2, 0, 0, 0, 0, 0, 82, 0, 26, 0, 167),
(22, 22, 747, 55, 38, 10, 0, 0, 0, 0, 4, 30, 0, 0, 0, 0, 0, 0, 23, 0, 2, 30, 79),
(23, 23, 51, 30, 0, 0, 0, 0, 0, 0, 48, 10, 0, 0, 10, 30, 0, 0, 0, 0, 0, 0, 0),
(24, 24, 27, 25, 0, 0, 0, 0, 0, 0, 0, 40, 0, 0, 25, 15, 0, 0, 0, 0, 0, 0, 0),
(25, 25, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 26, 1, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 55, 0, 0, 0, 0, 0, 0, 0),
(27, 27, 259, 0, 0, 0, 0, 0, 0, 0, 20, 35, 0, 0, 201, 10, 0, 0, 0, 0, 0, 0, 0),
(28, 28, 0, 0, 0, 0, 0, 0, 0, 0, 6, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 29, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 30, 7, 35, 0, 0, 0, 0, 0, 0, 2, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `types_avion`
--

CREATE TABLE IF NOT EXISTS `types_avion` (
  `id_avion` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  `type_avion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_avion`),
  KEY `types_avion` (`type_avion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Contenu de la table `types_avion`
--

INSERT INTO `types_avion` (`id_avion`, `type`, `type_avion`) VALUES
(1, 'A318', 2),
(2, 'A319', 2),
(3, 'A320', 2),
(4, 'A321', 2),
(5, 'A340', 2),
(6, 'A341', 2),
(7, 'A342', 2),
(8, 'AJET', 2),
(9, 'B737', 2),
(10, 'BE55', 2),
(11, 'CM170', 2),
(12, 'M20', 2),
(13, 'C152', 1),
(14, 'C172', 1),
(15, 'C177', 1),
(16, 'CAP10', 1),
(17, 'CO02', 1),
(18, 'Divers', 1),
(19, 'DR360', 1),
(20, 'DR400', 1),
(21, 'F1CR', 1),
(22, 'F1CT', 1),
(23, 'HR200', 1),
(24, 'J3', 1),
(25, 'M201', 1),
(26, 'M893', 1),
(27, 'MS880', 1),
(28, 'R1180', 1),
(29, 'R2100', 1),
(30, 'TB10', 1);

-- --------------------------------------------------------

--
-- Structure de la table `vols`
--

CREATE TABLE IF NOT EXISTS `vols` (
  `id` int(11) NOT NULL,
  `date_vol` int(11) NOT NULL,
  `nom_cdb` varchar(255) NOT NULL,
  `prenom_cdb` varchar(255) NOT NULL,
  `nom_opl` varchar(255) NOT NULL,
  `prenom_opl` varchar(255) NOT NULL,
  `nom_obs1` varchar(255) NOT NULL,
  `prenom_obs1` varchar(255) NOT NULL,
  `nom_obs2` varchar(255) NOT NULL,
  `prenom_obs2` varchar(255) NOT NULL,
  `nom_instructeur` varchar(255) NOT NULL,
  `prenom_instructeur` varchar(255) NOT NULL,
  `depart` varchar(4) NOT NULL,
  `arrivee` varchar(4) NOT NULL,
  `cdb_jour_heures` int(2) NOT NULL,
  `cdb_jour_minutes` int(2) NOT NULL,
  `cdb_nuit_heures` int(2) NOT NULL,
  `cdb_nuit_minutes` int(2) NOT NULL,
  `opl_jour_heures` int(2) NOT NULL,
  `opl_jour_minutes` int(2) NOT NULL,
  `opl_nuit_heures` int(2) NOT NULL,
  `opl_nuit_minutes` int(2) NOT NULL,
  `dc_jour_heures` int(2) NOT NULL,
  `dc_jour_minutes` int(2) NOT NULL,
  `dc_nuit_heures` int(2) NOT NULL,
  `dc_nuit_minutes` int(2) NOT NULL,
  `instructeur_jour_heures` int(2) NOT NULL,
  `instructeur_jour_minutes` int(2) NOT NULL,
  `instructeur_nuit_heures` int(2) NOT NULL,
  `instructeur_nuit_minutes` int(2) NOT NULL,
  `simu_heures` int(2) NOT NULL,
  `simu_minutes` int(2) NOT NULL,
  `ifr_heures` int(2) NOT NULL,
  `ifr_minutes` int(2) NOT NULL,
  `arr_ifr` int(2) NOT NULL,
  `fonction` varchar(10) NOT NULL,
  `type_avion` varchar(5) NOT NULL,
  `immat_id` varchar(11) NOT NULL,
  `observations` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `depart` (`depart`),
  KEY `arrivee` (`arrivee`),
  KEY `immat_id` (`immat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `vols`
--

INSERT INTO `vols` (`id`, `date_vol`, `nom_cdb`, `prenom_cdb`, `nom_opl`, `prenom_opl`, `nom_obs1`, `prenom_obs1`, `nom_obs2`, `prenom_obs2`, `nom_instructeur`, `prenom_instructeur`, `depart`, `arrivee`, `cdb_jour_heures`, `cdb_jour_minutes`, `cdb_nuit_heures`, `cdb_nuit_minutes`, `opl_jour_heures`, `opl_jour_minutes`, `opl_nuit_heures`, `opl_nuit_minutes`, `dc_jour_heures`, `dc_jour_minutes`, `dc_nuit_heures`, `dc_nuit_minutes`, `instructeur_jour_heures`, `instructeur_jour_minutes`, `instructeur_nuit_heures`, `instructeur_nuit_minutes`, `simu_heures`, `simu_minutes`, `ifr_heures`, `ifr_minutes`, `arr_ifr`, `fonction`, `type_avion`, `immat_id`, `observations`) VALUES
(1, 1384470000, 'Moi', '', 'Lui', '', 'Obersvateur 1', '', 'Obersvateur 1', '', 'instruct1', '', '1', '2', 2, 8, 1, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'pnf', 'vfr', '160', 'Vol normal'),
(2, 1384470000, 'Moi', '', 'Lui', '', 'Obersvateur 1', '', 'Obersvateur 1', '', 'instruct1', '', '1', '2', 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'pnf', 'vfr', '50', 'Vol normal'),
(3, 1384383600, 'Nicolas Renard', '', '', '', 'Theo Renard', '', '', '', 'Yvan Renard', '', '1', '3', 1, 20, 0, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 10, 0, 0, 2, 'pf', 'ifr', '179', 'Simulation de crash'),
(4, 1389654000, 'Papa', '', 'Moi', '', '', '', '', '', '', '', '1', '3', 1, 25, 3, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'pnf', 'ifr', '4', ''),
(5, 1389654000, 'Papa', '', 'Moi', '', '', '', '', '', '', '', '1', '3', 1, 25, 3, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'pnf', 'ifr', '4', ''),
(11, 1357081200, '', '', 'Sirac', 'Jean-Michel', '', '', '', '', '', '', 'CDG', 'VIE', 1, 21, 0, 56, 0, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PF', 'FGKXO', 'A320', 'ATR CAT III'),
(12, 1357081200, '', '', 'Sirac', 'Jean-Michel', '', '', '', '', '', '', 'VIE', 'CDG', 1, 52, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PNF', 'FGKXO', 'A320', ''),
(13, 1357081200, '', '', 'Sirac', 'Jean-Michel', '', '', '', '', '', '', 'CDG', 'LYS', 1, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PNF', 'FGKXO', 'A320', ''),
(14, 1357167600, '', '', 'Sirac', 'Jean-Michel', '', '', '', '', '', '', 'LYS', 'CDG', 0, 0, 1, 6, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PF', 'FGRHM', 'A319', 'ATR CAT III'),
(15, 1357167600, '', '', 'Sirac', 'Jean-Michel', '', '', '', '', '', '', 'CDG', 'TLV', 4, 35, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PNF', 'FGKXJ', 'A320', ''),
(16, 1357254000, '', '', 'Sirac', 'Jean-Michel', '', '', '', '', '', '', 'TLV', 'CDG', 4, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PF', 'FGKXY', 'A320', ''),
(17, 1357513200, '', '', 'Sirac', 'Jean-Michel', '', '', '', '', '', '', 'CDG', 'HAM', 1, 7, 0, 23, 0, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PNF', 'FGHQK', 'A320', ''),
(18, 1357513200, '', '', 'Sirac', 'Jean-Michel', '', '', '', '', '', '', 'HAM', 'CDG', 0, 0, 1, 29, 1, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PNF', 'FGHQK', 'A320', ''),
(19, 1357513200, '', '', 'Sirac', 'Jean-Michel', '', '', '', '', '', '', 'CDG', 'BES', 0, 0, 1, 9, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PF', 'FGUGP', 'A318', ''),
(20, 1357686000, '', '', 'Sirac', 'Jean-Michel', '', '', '', '', '', '', 'BES', 'CDG', 0, 0, 1, 21, 1, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PF', 'FGUGF', 'A318', ''),
(21, 1357686000, '', '', 'Sirac', 'Jean-Michel', '', '', '', '', '', '', 'CDG', 'SVO', 3, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PNF', 'FGKXU', 'A320', ''),
(22, 1357772400, '', '', 'Sirac', 'Jean-Michel', '', '', '', '', '', '', 'SVO', 'CDG', 1, 0, 2, 58, 2, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PNF', 'FGRHK', 'A319', 'ATR CAT III'),
(23, 1382652000, '', '', 'Saulais', 'Xavier', '', '', '', '', '', '', 'LYS', 'TLS', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PNF', 'FGUGD', 'A318', ''),
(24, 1382652000, '', '', 'Saulais', 'Xavier', '', '', '', '', '', '', 'TLS', 'LYS', 0, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PF', 'FGUGD', 'A318', ''),
(25, 1382652000, '', '', 'Saulais', 'Xavier', '', '', '', '', '', '', 'LYS', 'CDG', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PF', 'FGUGD', 'A318', ''),
(26, 1383260400, '', '', 'Feix', 'Daniel', '', '', '', '', '', '', 'CDG', 'AMS', 1, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PNF', 'FHEPB', 'A320', ''),
(27, 1383260400, '', '', 'Feix', 'Daniel', '', '', '', '', '', '', 'AMS', 'CDG', 1, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 'PF', 'FHEPB', 'A320', '');

-- --------------------------------------------------------

--
-- Structure de la table `vols_save`
--

CREATE TABLE IF NOT EXISTS `vols_save` (
  `id` int(11) NOT NULL DEFAULT '0',
  `date_vol` int(11) NOT NULL,
  `cdb` varchar(255) CHARACTER SET utf8 NOT NULL,
  `opl` varchar(255) CHARACTER SET utf8 NOT NULL,
  `obs1` varchar(255) CHARACTER SET utf8 NOT NULL,
  `obs2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `instructeur` varchar(255) CHARACTER SET utf8 NOT NULL,
  `depart` int(4) NOT NULL,
  `arrivee` int(4) NOT NULL,
  `hdv_jour_heures` int(2) NOT NULL,
  `hdv_jour_minutes` int(2) NOT NULL,
  `hdv_nuit_heures` int(2) NOT NULL,
  `hdv_nuit_minutes` int(2) NOT NULL,
  `simu` tinyint(1) NOT NULL,
  `simu_heures` int(2) NOT NULL,
  `simu_minutes` int(2) NOT NULL,
  `ifr_heures` int(2) NOT NULL,
  `ifr_minutes` int(2) NOT NULL,
  `arr_ifr` int(2) NOT NULL,
  `poste` varchar(5) CHARACTER SET utf8 NOT NULL,
  `fonction` varchar(10) CHARACTER SET utf8 NOT NULL,
  `type` varchar(5) CHARACTER SET utf8 NOT NULL,
  `immat_id` int(11) NOT NULL,
  `observations` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `vols_save`
--

INSERT INTO `vols_save` (`id`, `date_vol`, `cdb`, `opl`, `obs1`, `obs2`, `instructeur`, `depart`, `arrivee`, `hdv_jour_heures`, `hdv_jour_minutes`, `hdv_nuit_heures`, `hdv_nuit_minutes`, `simu`, `simu_heures`, `simu_minutes`, `ifr_heures`, `ifr_minutes`, `arr_ifr`, `poste`, `fonction`, `type`, `immat_id`, `observations`) VALUES
(1, 1384470000, 'Moi', 'Lui', 'Obersvateur 1', 'Obersvateur 1', 'instruct1', 1, 2, 2, 8, 1, 10, 0, 0, 0, 0, 0, 1, 'cdb', 'pnf', 'vfr', 160, 'Vol normal'),
(2, 1384470000, 'Moi', 'Lui', 'Obersvateur 1', 'Obersvateur 1', 'instruct1', 1, 2, 2, 0, 1, 1, 0, 0, 0, 0, 0, 1, 'cdb', 'pnf', 'vfr', 50, 'Vol normal'),
(3, 1384383600, 'Nicolas Renard', '', 'Theo Renard', '', 'Yvan Renard', 1, 3, 1, 20, 0, 50, 1, 2, 10, 0, 0, 2, 'cdb', 'pf', 'ifr', 179, 'Simulation de crash'),
(4, 1389654000, 'Papa', 'Moi', '', '', '', 1, 3, 1, 25, 3, 18, 0, 0, 0, 0, 0, 1, 'cdb', 'pnf', 'ifr', 4, ''),
(5, 1389654000, 'Papa', 'Moi', '', '', '', 1, 3, 1, 25, 3, 18, 0, 0, 0, 0, 0, 1, 'cdb', 'pnf', 'ifr', 4, '');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `immat_type`
--
ALTER TABLE `immat_type`
  ADD CONSTRAINT `types_avion_id_avion` FOREIGN KEY (`type`) REFERENCES `types_avion` (`id_avion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `recap`
--
ALTER TABLE `recap`
  ADD CONSTRAINT `types_avions_id_avion` FOREIGN KEY (`type_avion`) REFERENCES `types_avion` (`id_avion`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `types_avion`
--
ALTER TABLE `types_avion`
  ADD CONSTRAINT `nb_moteurs_type_avion` FOREIGN KEY (`type_avion`) REFERENCES `nb_moteurs_avion` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
