#!/bin/bash
date=$(date +%Y-%m-%d_%T)
if [[ -f './saveSQL/vol_latest.sql' ]]; then
  echo "Renommage du dernier fichier avec la date courante"
  mv -v ./saveSQL/vol_latest.sql ./saveSQL/vol_$date.sql
fi

echo "Export de la base de données"
mysqldump -u root -p vol > ./saveSQL/vol_latest.sql
