-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 11 Juin 2015 à 00:28
-- Version du serveur :  10.0.19-MariaDB-log
-- Version de PHP :  5.6.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `vol`
--

-- --------------------------------------------------------

--
-- Structure de la table `aita`
--

CREATE TABLE IF NOT EXISTS `aita` (
  `id_aita` int(5) NOT NULL,
  `code_aita` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `aita`
--

INSERT INTO `aita` (`id_aita`, `code_aita`) VALUES
(1, 'CDG'),
(2, 'VIE'),
(3, 'LYS'),
(4, 'TLV'),
(5, 'HAM'),
(6, 'BES'),
(7, 'SVO'),
(8, 'LIN'),
(9, 'WAW'),
(10, 'ORY'),
(11, 'NCE'),
(12, 'MPL'),
(13, 'TLN'),
(14, 'FRA'),
(15, 'CPH'),
(16, 'LIS'),
(17, 'VCE'),
(18, 'FCO'),
(19, 'AMS'),
(20, 'BCN'),
(21, 'RBA'),
(22, 'GVA'),
(23, 'PUF'),
(24, 'BIA'),
(25, 'OTP'),
(26, 'NTE'),
(27, 'BOD'),
(28, 'MAN'),
(29, 'ALG'),
(30, 'LED'),
(31, 'IST'),
(32, 'MAD'),
(33, 'CMN'),
(34, 'STR'),
(35, 'SOF'),
(36, 'LHR'),
(37, 'PTP'),
(38, 'ATH'),
(39, 'AJA'),
(40, 'TLS'),
(41, 'OPO'),
(42, 'MUC'),
(43, 'ARN'),
(44, 'BUD'),
(45, 'TUN'),
(47, 'SAJ');

-- --------------------------------------------------------

--
-- Structure de la table `fonction`
--

CREATE TABLE IF NOT EXISTS `fonction` (
  `id_fonction` int(11) NOT NULL,
  `fonction` varchar(10) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `fonction`
--

INSERT INTO `fonction` (`id_fonction`, `fonction`) VALUES
(1, 'PF'),
(2, 'PNF'),
(3, 'MIX');

-- --------------------------------------------------------

--
-- Structure de la table `immat`
--

CREATE TABLE IF NOT EXISTS `immat` (
  `id_immat` int(5) NOT NULL,
  `immat` varchar(10) NOT NULL,
  `type_avion_id` int(16) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `immat`
--

INSERT INTO `immat` (`id_immat`, `immat`, `type_avion_id`) VALUES
(1, 'FGFKA', 3),
(2, 'FGFKB', 3),
(3, 'FGFKD', 3),
(4, 'FGFKE', 3),
(5, 'FGFKF', 3),
(6, 'FGFKG', 3),
(7, 'FGFKH', 3),
(8, 'FGFKI', 3),
(9, 'FGFKJ', 3),
(10, 'FGFKJ', 3),
(11, 'FGFKK', 3),
(12, 'FGFKL', 3),
(13, 'FGFKM', 3),
(14, 'FGFKN', 3),
(15, 'FGFKO', 3),
(16, 'FGFKP', 3),
(17, 'FGFKQ', 3),
(18, 'FGFKR', 3),
(19, 'FGFKS', 3),
(20, 'FGFKT', 3),
(21, 'FGFKU', 3),
(22, 'FGFKV', 3),
(23, 'FGFKX', 3),
(24, 'FGFKY', 3),
(25, 'FGFKZ', 3),
(26, 'FGGEA', 3),
(27, 'FGGEB', 3),
(28, 'FGGEC', 3),
(29, 'FGGEE', 3),
(30, 'FGGEF', 3),
(31, 'FGGEG', 3),
(32, 'FGHQA', 3),
(33, 'FGHQB', 3),
(34, 'FGHQC', 3),
(35, 'FGHQD', 3),
(36, 'FGHQE', 3),
(37, 'FGHQF', 3),
(38, 'FGHQG', 3),
(39, 'FGHQH', 3),
(40, 'FGHQI', 3),
(41, 'FGHQJ', 3),
(42, 'FGHQK', 3),
(43, 'FGHQL', 3),
(44, 'FGHQM', 3),
(45, 'FGHQO', 3),
(46, 'FGHQP', 3),
(47, 'FGHQQ', 3),
(48, 'FGHQR', 3),
(49, 'FGJVA', 3),
(50, 'FGJVB', 3),
(51, 'FGJVG', 3),
(52, 'FGJVW', 3),
(53, 'FGKXA', 3),
(54, 'FGKXB', 3),
(55, 'FGKXC', 3),
(56, 'FGKXD', 3),
(57, 'FGKXE', 3),
(58, 'FGKXF', 3),
(59, 'FGKXG', 3),
(60, 'FGKXH', 3),
(61, 'FGKXI', 3),
(62, 'FGKXJ', 3),
(63, 'FGKXK', 3),
(64, 'FGKXL', 3),
(65, 'FGKXM', 3),
(66, 'FGKXN', 3),
(67, 'FGKXO', 3),
(68, 'FGKXP', 3),
(69, 'FGKXQ', 3),
(70, 'FGKXR', 3),
(71, 'FGKXS', 3),
(72, 'FGKXT', 3),
(73, 'FGKXU', 3),
(74, 'FGKXV', 3),
(75, 'FGKXY', 3),
(76, 'FGKXZ', 3),
(77, 'FGLGG', 3),
(78, 'FGLGH', 3),
(79, 'FGLGM', 3),
(80, 'FGMZA', 4),
(81, 'FGMZB', 4),
(82, 'FGMZC', 4),
(83, 'FGMZD', 4),
(84, 'FGMZE', 4),
(85, 'FGPMA', 2),
(86, 'FGPMB', 2),
(87, 'FGPMC', 2),
(88, 'FGPMD', 2),
(89, 'FGPME', 2),
(90, 'FGPMF', 2),
(91, 'FGPMG', 2),
(92, 'FGRHA', 2),
(93, 'FGRHB', 2),
(94, 'FGRHC', 2),
(95, 'FGRHD', 2),
(96, 'FGRHE', 2),
(97, 'FGRHF', 2),
(98, 'FGRHG', 2),
(99, 'FGRHH', 2),
(100, 'FGRHI', 2),
(101, 'FGRHJ', 2),
(102, 'FGRHK', 2),
(103, 'FGRHL', 2),
(104, 'FGRHM', 2),
(105, 'FGRHN', 2),
(106, 'FGRHO', 2),
(107, 'FGRHP', 2),
(108, 'FGRHQ', 2),
(109, 'FGRHR', 2),
(110, 'FGRHS', 2),
(111, 'FGRHT', 2),
(112, 'FGRHU', 2),
(113, 'FGRHV', 2),
(114, 'FGRHX', 2),
(115, 'FGRHY', 2),
(116, 'FGRHZ', 2),
(117, 'FGRXA', 2),
(118, 'FGRXB', 2),
(119, 'FGRXC', 2),
(120, 'FGRXD', 2),
(121, 'FGRXE', 2),
(122, 'FGRXF', 2),
(123, 'FGRXG', 2),
(124, 'FGRXH', 2),
(125, 'FGRXI', 2),
(126, 'FGRXJ', 2),
(127, 'FGRXK', 2),
(128, 'FGRXL', 2),
(129, 'FGRXM', 2),
(130, 'FGRXN', 2),
(131, 'FGRXR', 2),
(132, 'FGTAD', 4),
(133, 'FGTAE', 4),
(134, 'FGTAH', 4),
(135, 'FGTAI', 4),
(136, 'FGTAJ', 4),
(137, 'FGTAK', 4),
(138, 'FGTAL', 4),
(139, 'FGTAM', 4),
(140, 'FGTAN', 4),
(141, 'FGTAO', 4),
(142, 'FGTAP', 4),
(143, 'FGTAQ', 4),
(144, 'FGTAR', 4),
(145, 'FGTAS', 4),
(146, 'FGTAT', 4),
(147, 'FGTAU', 4),
(148, 'FGTAV', 4),
(149, 'FGTAX', 4),
(150, 'FGTAY', 4),
(151, 'FGTAZ', 4),
(152, 'FGUGA', 1),
(153, 'FGUGB', 1),
(154, 'FGUGC', 1),
(155, 'FGUGD', 1),
(156, 'FGUGE', 1),
(157, 'FGUGF', 1),
(158, 'FGUGG', 1),
(159, 'FGUGH', 1),
(160, 'FGUGI', 1),
(161, 'FGUGJ', 1),
(162, 'FGUGK', 1),
(163, 'FGUGL', 1),
(164, 'FGUGM', 1),
(165, 'FGUGN', 1),
(166, 'FGUGO', 1),
(167, 'FGUGP', 1),
(168, 'FGUGQ', 1),
(169, 'FGUGR', 1),
(170, 'FHBNA', 3),
(171, 'FHBNC', 3),
(172, 'FHBND', 3),
(173, 'FHBNG', 3),
(174, 'FHEPA', 3),
(175, 'FHEPB', 3),
(176, 'FHEPC', 3),
(177, 'FHEPD', 3),
(178, 'FHEPE', 3),
(179, 'simu318', 1),
(180, 'simu319', 2),
(181, 'simu320', 3),
(182, 'simu321', 4),
(189, 'simuA318', 1),
(190, 'simuA319', 2),
(191, 'simuA320', 3),
(192, 'simuA312', 4);

-- --------------------------------------------------------

--
-- Structure de la table `nb_moteurs`
--

CREATE TABLE IF NOT EXISTS `nb_moteurs` (
  `id_nb_moteurs` int(5) NOT NULL,
  `nb_moteurs` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `nb_moteurs`
--

INSERT INTO `nb_moteurs` (`id_nb_moteurs`, `nb_moteurs`) VALUES
(1, 'Monomoteur'),
(2, 'Multimoteur');

-- --------------------------------------------------------

--
-- Structure de la table `pilote`
--

CREATE TABLE IF NOT EXISTS `pilote` (
  `id` int(11) NOT NULL,
  `prenom_pilote` varchar(255) CHARACTER SET utf8 NOT NULL,
  `nom_pilote` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pilote`
--

INSERT INTO `pilote` (`id`, `prenom_pilote`, `nom_pilote`) VALUES
(1, 'Yvan', 'Renard'),
(2, 'Doudou', 'D''amour'),
(3, 'Matthieu', 'Bousendorfer'),
(4, 'Arnaud', 'Ked'),
(5, 'Nicolas', 'Renard');

-- --------------------------------------------------------

--
-- Structure de la table `pilote_test`
--

CREATE TABLE IF NOT EXISTS `pilote_test` (
  `id` int(11) NOT NULL,
  `prenom_pilote` varchar(255) CHARACTER SET utf8 NOT NULL,
  `nom_pilote` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `poste`
--

CREATE TABLE IF NOT EXISTS `poste` (
  `id_poste` int(5) NOT NULL,
  `poste` varchar(255) CHARACTER SET utf8 NOT NULL,
  `poste_abbr` varchar(10) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `poste`
--

INSERT INTO `poste` (`id_poste`, `poste`, `poste_abbr`) VALUES
(1, 'Commandant de bord', 'cdb'),
(2, 'Copilote', 'opl'),
(3, 'Instructeur', 'instruct'),
(4, 'Observateur', 'obs');

-- --------------------------------------------------------

--
-- Structure de la table `type_avion`
--

CREATE TABLE IF NOT EXISTS `type_avion` (
  `id_avion` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `nb_moteurs_id` int(3) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `type_avion`
--

INSERT INTO `type_avion` (`id_avion`, `type`, `nb_moteurs_id`) VALUES
(1, 'A318', 2),
(2, 'A319', 2),
(3, 'A320', 2),
(4, 'A321', 2),
(5, 'A340', 2),
(6, 'A341', 2),
(7, 'A342', 2),
(8, 'AJET', 2),
(9, 'B737', 2),
(10, 'BE55', 2),
(11, 'CM170', 2),
(12, 'M20', 2),
(13, 'C152', 1),
(14, 'C172', 1),
(15, 'C177', 1),
(16, 'CAP10', 1),
(17, 'CO02', 1),
(18, 'Divers', 1),
(19, 'DR360', 1),
(20, 'DR400', 1),
(21, 'F1CR', 1),
(22, 'F1CT', 1),
(23, 'HR200', 1),
(24, 'J3', 1),
(25, 'M201', 1),
(26, 'M893', 1),
(27, 'MS880', 1),
(28, 'R1180', 1),
(29, 'R2100', 1),
(30, 'TB10', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(16) NOT NULL,
  `username` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`user_id`, `username`, `pass`, `salt`) VALUES
(1, 'fox', 'fox', 'fox');

-- --------------------------------------------------------

--
-- Structure de la table `vols`
--

CREATE TABLE IF NOT EXISTS `vols` (
  `id_vol` int(10) NOT NULL,
  `date_vol` date DEFAULT NULL,
  `cdb_id` int(5) NOT NULL,
  `opl_id` int(5) NOT NULL,
  `obs1_id` int(5) NOT NULL,
  `obs2_id` int(5) NOT NULL,
  `instructeur_id` int(5) NOT NULL,
  `depart_aita_id` int(5) NOT NULL,
  `arrivee_aita_id` int(5) NOT NULL,
  `duree_jour` time NOT NULL,
  `duree_nuit` time NOT NULL,
  `arrivee_ifr` int(5) NOT NULL,
  `fonction_id` int(3) NOT NULL,
  `poste_id` int(3) NOT NULL,
  `immat_id` int(5) NOT NULL,
  `observations` text NOT NULL,
  `simu` tinyint(1) DEFAULT NULL,
  `user_id` int(16) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `aita`
--
ALTER TABLE `aita`
  ADD PRIMARY KEY (`id_aita`);

--
-- Index pour la table `fonction`
--
ALTER TABLE `fonction`
  ADD PRIMARY KEY (`id_fonction`);

--
-- Index pour la table `immat`
--
ALTER TABLE `immat`
  ADD PRIMARY KEY (`id_immat`), ADD KEY `type` (`type_avion_id`);

--
-- Index pour la table `nb_moteurs`
--
ALTER TABLE `nb_moteurs`
  ADD PRIMARY KEY (`id_nb_moteurs`), ADD KEY `nb_moteurs_avion` (`nb_moteurs`);

--
-- Index pour la table `pilote`
--
ALTER TABLE `pilote`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pilote_test`
--
ALTER TABLE `pilote_test`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `poste`
--
ALTER TABLE `poste`
  ADD PRIMARY KEY (`id_poste`);

--
-- Index pour la table `type_avion`
--
ALTER TABLE `type_avion`
  ADD PRIMARY KEY (`id_avion`), ADD KEY `types_avion` (`nb_moteurs_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`), ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `vols`
--
ALTER TABLE `vols`
  ADD PRIMARY KEY (`id_vol`), ADD KEY `depart` (`depart_aita_id`), ADD KEY `arrivee` (`arrivee_aita_id`), ADD KEY `cdb_id` (`cdb_id`), ADD KEY `opl_id` (`opl_id`), ADD KEY `obs1_id` (`obs1_id`), ADD KEY `obs2_id` (`obs2_id`), ADD KEY `instructeur_id` (`instructeur_id`), ADD KEY `depart_aita_id` (`depart_aita_id`), ADD KEY `arrivee_aita_id` (`arrivee_aita_id`), ADD KEY `fonction_id` (`fonction_id`), ADD KEY `poste_id` (`poste_id`), ADD KEY `immat_id` (`immat_id`), ADD KEY `user_id` (`user_id`), ADD KEY `user_id_2` (`user_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `aita`
--
ALTER TABLE `aita`
  MODIFY `id_aita` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT pour la table `fonction`
--
ALTER TABLE `fonction`
  MODIFY `id_fonction` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `immat`
--
ALTER TABLE `immat`
  MODIFY `id_immat` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=193;
--
-- AUTO_INCREMENT pour la table `nb_moteurs`
--
ALTER TABLE `nb_moteurs`
  MODIFY `id_nb_moteurs` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `pilote`
--
ALTER TABLE `pilote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `pilote_test`
--
ALTER TABLE `pilote_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `poste`
--
ALTER TABLE `poste`
  MODIFY `id_poste` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `type_avion`
--
ALTER TABLE `type_avion`
  MODIFY `id_avion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(16) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `vols`
--
ALTER TABLE `vols`
  MODIFY `id_vol` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `immat`
--
ALTER TABLE `immat`
ADD CONSTRAINT `type_avion_id` FOREIGN KEY (`type_avion_id`) REFERENCES `type_avion` (`id_avion`);

--
-- Contraintes pour la table `type_avion`
--
ALTER TABLE `type_avion`
ADD CONSTRAINT `nb_moteurs_id` FOREIGN KEY (`nb_moteurs_id`) REFERENCES `nb_moteurs` (`id_nb_moteurs`);

--
-- Contraintes pour la table `vols`
--
ALTER TABLE `vols`
ADD CONSTRAINT `arrivee_aita_id` FOREIGN KEY (`arrivee_aita_id`) REFERENCES `aita` (`id_aita`),
ADD CONSTRAINT `cdb_id` FOREIGN KEY (`cdb_id`) REFERENCES `pilote` (`id`),
ADD CONSTRAINT `depart_aita_id` FOREIGN KEY (`depart_aita_id`) REFERENCES `aita` (`id_aita`),
ADD CONSTRAINT `fonction_id` FOREIGN KEY (`fonction_id`) REFERENCES `fonction` (`id_fonction`),
ADD CONSTRAINT `immat_id` FOREIGN KEY (`immat_id`) REFERENCES `immat` (`id_immat`),
ADD CONSTRAINT `instructeur_id` FOREIGN KEY (`instructeur_id`) REFERENCES `pilote` (`id`),
ADD CONSTRAINT `obs1_id` FOREIGN KEY (`obs1_id`) REFERENCES `pilote` (`id`),
ADD CONSTRAINT `obs2_id` FOREIGN KEY (`obs2_id`) REFERENCES `pilote` (`id`),
ADD CONSTRAINT `opl_id` FOREIGN KEY (`opl_id`) REFERENCES `pilote` (`id`),
ADD CONSTRAINT `poste_id` FOREIGN KEY (`poste_id`) REFERENCES `poste` (`id_poste`),
ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
