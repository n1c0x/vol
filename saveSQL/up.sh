#!/bin/bash
echo "Creation de la base de données vol"
echo "CREATE DATABASE IF NOT EXISTS vol" | mysql -u root -p
echo "Creation de l'utilisateur vol"
echo "CREATE USER 'vol'@'localhost' IDENTIFIED BY 'uq2uwYCjSJSyG5zd';" | mysql -u root -p
echo "Assignation des droits: GRANT ALL PRIVILEGES ON vol.* TO 'vol'@'localhost';"
echo "GRANT ALL PRIVILEGES ON vol.* TO 'vol'@'localhost';" | mysql -u root -p
echo "Activation des droits: FLUSH PRIVILEGES"
echo "FLUSH PRIVILEGES" | mysql -u root -p
echo 'Import de la base de données du fichier vol_latest.sql'
mysql -u root -p vol < vol_latest.sql
