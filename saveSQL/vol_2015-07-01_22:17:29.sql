-- MySQL dump 10.15  Distrib 10.0.20-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: vol
-- ------------------------------------------------------
-- Server version	10.0.20-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aita`
--

DROP TABLE IF EXISTS `aita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aita` (
  `id_aita` int(5) NOT NULL AUTO_INCREMENT,
  `code_aita` varchar(5) NOT NULL,
  PRIMARY KEY (`id_aita`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aita`
--

LOCK TABLES `aita` WRITE;
/*!40000 ALTER TABLE `aita` DISABLE KEYS */;
INSERT INTO `aita` VALUES (1,'CDG'),(2,'VIE'),(3,'LYS'),(4,'TLV'),(5,'HAM'),(6,'BES'),(7,'SVO'),(8,'LIN'),(9,'WAW'),(10,'ORY'),(11,'NCE'),(12,'MPL'),(13,'TLN'),(14,'FRA'),(15,'CPH'),(16,'LIS'),(17,'VCE'),(18,'FCO'),(19,'AMS'),(20,'BCN'),(21,'RBA'),(22,'GVA'),(23,'PUF'),(24,'BIA'),(25,'OTP'),(26,'NTE'),(27,'BOD'),(28,'MAN'),(29,'ALG'),(30,'LED'),(31,'IST'),(32,'MAD'),(33,'CMN'),(34,'STR'),(35,'SOF'),(36,'LHR'),(37,'PTP'),(38,'ATH'),(39,'AJA'),(40,'TLS'),(41,'OPO'),(42,'MUC'),(43,'ARN'),(44,'BUD'),(45,'TUN'),(47,'SAJ');
/*!40000 ALTER TABLE `aita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fonction`
--

DROP TABLE IF EXISTS `fonction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fonction` (
  `id_fonction` int(11) NOT NULL AUTO_INCREMENT,
  `fonction` varchar(10) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_fonction`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fonction`
--

LOCK TABLES `fonction` WRITE;
/*!40000 ALTER TABLE `fonction` DISABLE KEYS */;
INSERT INTO `fonction` VALUES (1,'PF'),(2,'PNF'),(3,'MIX');
/*!40000 ALTER TABLE `fonction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ifr_vfr`
--

DROP TABLE IF EXISTS `ifr_vfr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ifr_vfr` (
  `id_ifr_vfr` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(6) NOT NULL,
  PRIMARY KEY (`id_ifr_vfr`),
  KEY `id_ifr_vfr` (`id_ifr_vfr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ifr_vfr`
--

LOCK TABLES `ifr_vfr` WRITE;
/*!40000 ALTER TABLE `ifr_vfr` DISABLE KEYS */;
INSERT INTO `ifr_vfr` VALUES (1,'IFR'),(2,'VFR');
/*!40000 ALTER TABLE `ifr_vfr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `immat`
--

DROP TABLE IF EXISTS `immat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `immat` (
  `id_immat` int(5) NOT NULL AUTO_INCREMENT,
  `immat` varchar(10) NOT NULL,
  `type_avion_id` int(16) NOT NULL,
  PRIMARY KEY (`id_immat`),
  KEY `type` (`type_avion_id`),
  CONSTRAINT `type_avion_id` FOREIGN KEY (`type_avion_id`) REFERENCES `type_avion` (`id_avion`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `immat`
--

LOCK TABLES `immat` WRITE;
/*!40000 ALTER TABLE `immat` DISABLE KEYS */;
INSERT INTO `immat` VALUES (1,'FGFKA',3),(2,'FGFKB',3),(3,'FGFKD',3),(4,'FGFKE',3),(5,'FGFKF',3),(6,'FGFKG',3),(7,'FGFKH',3),(8,'FGFKI',3),(9,'FGFKJ',3),(10,'FGFKJ',3),(11,'FGFKK',3),(12,'FGFKL',3),(13,'FGFKM',3),(14,'FGFKN',3),(15,'FGFKO',3),(16,'FGFKP',3),(17,'FGFKQ',3),(18,'FGFKR',3),(19,'FGFKS',3),(20,'FGFKT',3),(21,'FGFKU',3),(22,'FGFKV',3),(23,'FGFKX',3),(24,'FGFKY',3),(25,'FGFKZ',3),(26,'FGGEA',3),(27,'FGGEB',3),(28,'FGGEC',3),(29,'FGGEE',3),(30,'FGGEF',3),(31,'FGGEG',3),(32,'FGHQA',3),(33,'FGHQB',3),(34,'FGHQC',3),(35,'FGHQD',3),(36,'FGHQE',3),(37,'FGHQF',3),(38,'FGHQG',3),(39,'FGHQH',3),(40,'FGHQI',3),(41,'FGHQJ',3),(42,'FGHQK',3),(43,'FGHQL',3),(44,'FGHQM',3),(45,'FGHQO',3),(46,'FGHQP',3),(47,'FGHQQ',3),(48,'FGHQR',3),(49,'FGJVA',3),(50,'FGJVB',3),(51,'FGJVG',3),(52,'FGJVW',3),(53,'FGKXA',3),(54,'FGKXB',3),(55,'FGKXC',3),(56,'FGKXD',3),(57,'FGKXE',3),(58,'FGKXF',3),(59,'FGKXG',3),(60,'FGKXH',3),(61,'FGKXI',3),(62,'FGKXJ',3),(63,'FGKXK',3),(64,'FGKXL',3),(65,'FGKXM',3),(66,'FGKXN',3),(67,'FGKXO',3),(68,'FGKXP',3),(69,'FGKXQ',3),(70,'FGKXR',3),(71,'FGKXS',3),(72,'FGKXT',3),(73,'FGKXU',3),(74,'FGKXV',3),(75,'FGKXY',3),(76,'FGKXZ',3),(77,'FGLGG',3),(78,'FGLGH',3),(79,'FGLGM',3),(80,'FGMZA',4),(81,'FGMZB',4),(82,'FGMZC',4),(83,'FGMZD',4),(84,'FGMZE',4),(85,'FGPMA',2),(86,'FGPMB',2),(87,'FGPMC',2),(88,'FGPMD',2),(89,'FGPME',2),(90,'FGPMF',2),(91,'FGPMG',2),(92,'FGRHA',2),(93,'FGRHB',2),(94,'FGRHC',2),(95,'FGRHD',2),(96,'FGRHE',2),(97,'FGRHF',2),(98,'FGRHG',2),(99,'FGRHH',2),(100,'FGRHI',2),(101,'FGRHJ',2),(102,'FGRHK',2),(103,'FGRHL',2),(104,'FGRHM',2),(105,'FGRHN',2),(106,'FGRHO',2),(107,'FGRHP',2),(108,'FGRHQ',2),(109,'FGRHR',2),(110,'FGRHS',2),(111,'FGRHT',2),(112,'FGRHU',2),(113,'FGRHV',2),(114,'FGRHX',2),(115,'FGRHY',2),(116,'FGRHZ',2),(117,'FGRXA',2),(118,'FGRXB',2),(119,'FGRXC',2),(120,'FGRXD',2),(121,'FGRXE',2),(122,'FGRXF',2),(123,'FGRXG',2),(124,'FGRXH',2),(125,'FGRXI',2),(126,'FGRXJ',2),(127,'FGRXK',2),(128,'FGRXL',2),(129,'FGRXM',2),(130,'FGRXN',2),(131,'FGRXR',2),(132,'FGTAD',4),(133,'FGTAE',4),(134,'FGTAH',4),(135,'FGTAI',4),(136,'FGTAJ',4),(137,'FGTAK',4),(138,'FGTAL',4),(139,'FGTAM',4),(140,'FGTAN',4),(141,'FGTAO',4),(142,'FGTAP',4),(143,'FGTAQ',4),(144,'FGTAR',4),(145,'FGTAS',4),(146,'FGTAT',4),(147,'FGTAU',4),(148,'FGTAV',4),(149,'FGTAX',4),(150,'FGTAY',4),(151,'FGTAZ',4),(152,'FGUGA',1),(153,'FGUGB',1),(154,'FGUGC',1),(155,'FGUGD',1),(156,'FGUGE',1),(157,'FGUGF',1),(158,'FGUGG',1),(159,'FGUGH',1),(160,'FGUGI',1),(161,'FGUGJ',1),(162,'FGUGK',1),(163,'FGUGL',1),(164,'FGUGM',1),(165,'FGUGN',1),(166,'FGUGO',1),(167,'FGUGP',1),(168,'FGUGQ',1),(169,'FGUGR',1),(170,'FHBNA',3),(171,'FHBNC',3),(172,'FHBND',3),(173,'FHBNG',3),(174,'FHEPA',3),(175,'FHEPB',3),(176,'FHEPC',3),(177,'FHEPD',3),(178,'FHEPE',3),(179,'simu318',1),(180,'simu319',2),(181,'simu320',3),(182,'simu321',4),(189,'simuA318',1),(190,'simuA319',2),(191,'simuA320',3),(192,'simuA312',4);
/*!40000 ALTER TABLE `immat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nb_moteurs`
--

DROP TABLE IF EXISTS `nb_moteurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nb_moteurs` (
  `id_nb_moteurs` int(5) NOT NULL AUTO_INCREMENT,
  `nb_moteurs` varchar(255) NOT NULL,
  PRIMARY KEY (`id_nb_moteurs`),
  KEY `nb_moteurs_avion` (`nb_moteurs`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nb_moteurs`
--

LOCK TABLES `nb_moteurs` WRITE;
/*!40000 ALTER TABLE `nb_moteurs` DISABLE KEYS */;
INSERT INTO `nb_moteurs` VALUES (1,'Monomoteur'),(2,'Multimoteur');
/*!40000 ALTER TABLE `nb_moteurs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pilote`
--

DROP TABLE IF EXISTS `pilote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pilote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom_pilote` varchar(255) CHARACTER SET utf8 NOT NULL,
  `nom_pilote` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pilote`
--

LOCK TABLES `pilote` WRITE;
/*!40000 ALTER TABLE `pilote` DISABLE KEYS */;
INSERT INTO `pilote` VALUES (1,'Yvan','Renard'),(2,'Doudou','D\'amour'),(3,'Matthieu','Bousendorfer'),(4,'Arnaud','Ked'),(5,'Nicolas','Renard');
/*!40000 ALTER TABLE `pilote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pilote_test`
--

DROP TABLE IF EXISTS `pilote_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pilote_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom_pilote` varchar(255) CHARACTER SET utf8 NOT NULL,
  `nom_pilote` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pilote_test`
--

LOCK TABLES `pilote_test` WRITE;
/*!40000 ALTER TABLE `pilote_test` DISABLE KEYS */;
/*!40000 ALTER TABLE `pilote_test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poste`
--

DROP TABLE IF EXISTS `poste`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poste` (
  `id_poste` int(5) NOT NULL AUTO_INCREMENT,
  `poste` varchar(255) CHARACTER SET utf8 NOT NULL,
  `poste_abbr` varchar(10) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_poste`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poste`
--

LOCK TABLES `poste` WRITE;
/*!40000 ALTER TABLE `poste` DISABLE KEYS */;
INSERT INTO `poste` VALUES (1,'Commandant de bord','cdb'),(2,'Copilote','opl'),(3,'Instructeur','instruct'),(4,'Observateur','obs');
/*!40000 ALTER TABLE `poste` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_avion`
--

DROP TABLE IF EXISTS `type_avion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_avion` (
  `id_avion` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  `nb_moteurs_id` int(3) DEFAULT NULL,
  PRIMARY KEY (`id_avion`),
  KEY `types_avion` (`nb_moteurs_id`),
  CONSTRAINT `nb_moteurs_id` FOREIGN KEY (`nb_moteurs_id`) REFERENCES `nb_moteurs` (`id_nb_moteurs`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_avion`
--

LOCK TABLES `type_avion` WRITE;
/*!40000 ALTER TABLE `type_avion` DISABLE KEYS */;
INSERT INTO `type_avion` VALUES (1,'A318',2),(2,'A319',2),(3,'A320',2),(4,'A321',2),(5,'A340',2),(6,'A341',2),(7,'A342',2),(8,'AJET',2),(9,'B737',2),(10,'BE55',2),(11,'CM170',2),(12,'M20',2),(13,'C152',1),(14,'C172',1),(15,'C177',1),(16,'CAP10',1),(17,'CO02',1),(18,'Divers',1),(19,'DR360',1),(20,'DR400',1),(21,'F1CR',1),(22,'F1CT',1),(23,'HR200',1),(24,'J3',1),(25,'M201',1),(26,'M893',1),(27,'MS880',1),(28,'R1180',1),(29,'R2100',1),(30,'TB10',1);
/*!40000 ALTER TABLE `type_avion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(16) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'fox','fox','fox');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vols`
--

DROP TABLE IF EXISTS `vols`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vols` (
  `id_vol` int(10) NOT NULL AUTO_INCREMENT,
  `date_vol` date DEFAULT NULL,
  `cdb_id` varchar(255) NOT NULL,
  `opl_id` varchar(255) NOT NULL,
  `obs1_id` varchar(255) NOT NULL,
  `obs2_id` varchar(255) NOT NULL,
  `instructeur_id` varchar(255) NOT NULL,
  `depart_aita_id` int(5) NOT NULL,
  `arrivee_aita_id` int(5) NOT NULL,
  `duree_jour` time NOT NULL,
  `duree_nuit` time NOT NULL,
  `arrivee_ifr` int(5) NOT NULL,
  `fonction_id` int(3) NOT NULL,
  `poste_id` int(3) NOT NULL,
  `immat_id` int(5) NOT NULL,
  `ifr_vfr_id` int(3) NOT NULL,
  `observations` text NOT NULL,
  `simu` tinyint(1) DEFAULT NULL,
  `dc` tinyint(4) DEFAULT NULL,
  `user_id` int(16) NOT NULL,
  PRIMARY KEY (`id_vol`),
  KEY `depart` (`depart_aita_id`),
  KEY `arrivee` (`arrivee_aita_id`),
  KEY `cdb_id` (`cdb_id`),
  KEY `opl_id` (`opl_id`),
  KEY `obs1_id` (`obs1_id`),
  KEY `obs2_id` (`obs2_id`),
  KEY `instructeur_id` (`instructeur_id`),
  KEY `depart_aita_id` (`depart_aita_id`),
  KEY `arrivee_aita_id` (`arrivee_aita_id`),
  KEY `fonction_id` (`fonction_id`),
  KEY `poste_id` (`poste_id`),
  KEY `immat_id` (`immat_id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`),
  KEY `ifr_vfr_id` (`ifr_vfr_id`),
  CONSTRAINT `arrivee_aita_id` FOREIGN KEY (`arrivee_aita_id`) REFERENCES `aita` (`id_aita`),
  CONSTRAINT `depart_aita_id` FOREIGN KEY (`depart_aita_id`) REFERENCES `aita` (`id_aita`),
  CONSTRAINT `fonction_id` FOREIGN KEY (`fonction_id`) REFERENCES `fonction` (`id_fonction`),
  CONSTRAINT `ifr_vfr_id` FOREIGN KEY (`ifr_vfr_id`) REFERENCES `ifr_vfr` (`id_ifr_vfr`),
  CONSTRAINT `immat_id` FOREIGN KEY (`immat_id`) REFERENCES `immat` (`id_immat`),
  CONSTRAINT `poste_id` FOREIGN KEY (`poste_id`) REFERENCES `poste` (`id_poste`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vols`
--

LOCK TABLES `vols` WRITE;
/*!40000 ALTER TABLE `vols` DISABLE KEYS */;
INSERT INTO `vols` VALUES (1,'2015-06-12',' ','Nicolas Renard','Anne-sophie Schlick','Yvan Renard',' ',29,44,'05:12:00','01:26:00',1,3,1,7,1,'Vol très cool',NULL,1,1),(2,'2015-06-14',' ','Nicolas Renard','Anne-sophie Schlick','Yvan Renard',' ',29,44,'05:12:00','01:26:00',1,3,1,7,2,'Observations très utiles, qui sont aussi vachement longues, parce que c\'est important d\'avoir plein d\'informations',1,NULL,1),(3,'2015-06-16',' ','Nicolas Renard','Anne-sophie Schlick','Yvan Renard','Matthieu Bousendorfer',29,44,'05:22:00','01:48:00',1,3,1,7,1,'Mauvais temps',1,NULL,1);
/*!40000 ALTER TABLE `vols` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-01 22:12:09
